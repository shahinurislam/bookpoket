<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Artist;
use Illuminate\Support\Facades\File; 


class ArtistsController extends Controller
{
    //--login system
    public function __construct()
    {
        $this->middleware('auth');
    }
    //artists
    public function index()
    {
        $artists = Artist::all();
        return view('artists.index',compact('artists'));
    }
    public function create()
    {
        return view('artists.create');
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'user_id' => 'required', 
            'title' => 'required', 
            'content' => 'required', 
            'thumbnail_path' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
            'slug' => 'unique:posts', 
            'status' => 'required', 
        ]);

        //$slug = Str::slug($request->name, '-');
        $slug= $this->createSlug($request->title);
        $input = $request->all();
        $input['slug'] = "$slug";

        //for image uploads
        $imageName = time().'.'.$request->thumbnail_path->extension();  
        //$request->thumbnail_path->move(public_path('images/artists'), $imageName);
        $request->thumbnail_path->move('images/artists/', $imageName);
       // $input = $request->all();
        $input['thumbnail_path'] = "$imageName";  

        Artist::create($input);

        session()->flash('message','Data insert successfully');
        return redirect('/dashboard/artists');

    }
    // create diffrent slug
    public function createSlug($title, $id = 0)
    {
        $slug = Str::slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Artist::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
    // slug---

    public function show($id)
    {
        $artist = Artist::find($id);
        return view('artists.artist-single',compact('artist'));
    }
    public function edit($id)
    {
        $artist = Artist::find($id);
        return view('artists.edit',compact('artist'));
    }
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'thumbnail_path' => '',
        ]);
        
        $artist = Artist::find($id);

        //dd($request->thumbnail_path);
        //img
        if($request->thumbnail_path == null){   
            $input = $request->all();
            $input['thumbnail_path'] = $request->thumbnail_path_re; //replease by value
            $artist->update($input);
        }else{
            unlink("images/artists/".$artist->thumbnail_path);
            //for image uploads
            $imageName = time().'.'.$request->thumbnail_path->extension();  
            $request->thumbnail_path->move('images/artists/', $imageName);
            $input = $request->all();
            $input['thumbnail_path'] = "$imageName";
            $artist->update($input);
        }
        session()->flash('message','Data update successfully');
        return redirect('/dashboard/artists');
    }
    public function destroy($id)
    {
        //img distroy
        $artist = Artist::find($id);
        if($artist->thumbnail_path){
            unlink("images/artists/".$artist->thumbnail_path);
            //File::delete(public_path("images/artists/".$artist->thumbnail_path));
        }else{}

        Artist::destroy($id); 

        session()->flash('messageDestroy','Data Delete successfully');
        return redirect('/dashboard/artists');

    }
}
