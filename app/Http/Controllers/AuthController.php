<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Config\auth;

class AuthController extends Controller
{
    //register
    public function showRegisterForm(){
        return view('signup');
    }
    public function prosessRegister(Request $request){
        //validation
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'usr_role' => ''
        ]);
        $data = [
            'name' => $request->input('name'),
            'email' => strtolower($request->input('email')),
            'password' => bcrypt($request->input('password')),
            'usr_role' => strtolower("0"),
        ];
        try{
            User::create($data);
            $this->setSuccessfullyMessage('User account created.');
            return redirect()->route('signin');
        }catch(Exeption $e){
            $this->setErrorMessage($e->getMessage());
            return redirect()->back();
        }

     }
    //login
    public function showLoginForm(){
        return view('signin'); 
    }
    public function processLogin(Request $request){
         //validation
         $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        $credentials = $request->except(['_token']);
        if (auth()->attempt($credentials)){
            return redirect('/');
        }
        $this->setErrorMessage('Invalid credential'); 
        return redirect()->back();
    }

    public function logout(){
        
        auth()->logout();
        $this->setSuccessfullyMessage('User has been logged-out.');
        //return redirect()->route('/signin');
        return view('signin'); 
    }
}
