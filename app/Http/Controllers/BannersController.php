<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use Illuminate\Support\Str;
use App\Models\Banner;

class BannersController extends Controller
{
    //artists
    public function index()
    {
        $banners = Banner::all();
        return view('banners.index',compact('banners',));
    }
    public function create()
    {
        return view('banners.create');
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'user_id' => 'required', 
            'title' => 'required', 
            'content' => 'required', 
            'thumbnail_path' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
            'slug' => 'unique:posts', 
            'status' => 'required', 
        ]);

        //$slug = Str::slug($request->name, '-');
        $slug= $this->createSlug($request->title);
        $input = $request->all();
        $input['slug'] = "$slug";

        //for image uploads
        $imageName = time().'.'.$request->thumbnail_path->extension();  
        $request->thumbnail_path->move('images/banners/', $imageName);
       // $input = $request->all();
        $input['thumbnail_path'] = "$imageName";  

        Banner::create($input);

        session()->flash('message','Data insert successfully');
        return redirect('/dashboard/banners');

    }
    // create diffrent slug
    public function createSlug($title, $id = 0)
    {
        $slug = Str::slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Banner::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
    // slug---

    public function show($id)
    {
        $banner = Banner::find($id);
        return view('banners.banner-single',compact('banner'));
    }
    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('banners.edit',compact('banner'));
    }
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'thumbnail_path' => '',
        ]);
        
        $banner = Banner::find($id);

        //dd($request->thumbnail_path);
        //img
        if($request->thumbnail_path == null){   
            $input = $request->all();
            $input['thumbnail_path'] = $request->thumbnail_path_re; //replease by value
            $banner->update($input);
        }else{
            unlink("images/banners/".$banner->thumbnail_path);
            //for image uploads
            $imageName = time().'.'.$request->thumbnail_path->extension();  
            $request->thumbnail_path->move('images/banners/', $imageName);
            $input = $request->all();
            $input['thumbnail_path'] = "$imageName";
            $banner->update($input);
        }
        session()->flash('message','Data update successfully');
        return redirect('/dashboard/banners');
    }
    public function destroy($id)
    {
        //img distroy
        $banner = Banner::find($id);
        if($banner->thumbnail_path){
            unlink("images/banners/".$banner->thumbnail_path);
        }else{}

        Banner::destroy($id); 

        session()->flash('messageDestroy','Data Delete successfully');
        return redirect('/dashboard/banners');

    }
}
