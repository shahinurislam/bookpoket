<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;

class CommentsController extends Controller
{
    
    public function store(Request $request)
    {
        $validated = $request->validate([
            'post_id' => 'required', 
            'user_id' => 'required', 
            'reply_id' => '', 
            'comment' => 'required',            
            'changeQuote' => ''
        ]);
        $input = $request->all();
        $input['comment'] = $request->changeQuote.$request->comment; //coment and change comment add

        try{
            $input = Comment::create($input);

            session()->flash('message'.$input->id,'success');        
            return redirect('/post/listen/'.$request->post_id.'#'.$input->id);

            //$this->setSuccessfullyMessage('Data insert successfully.');
            //return redirect('/post/listen/'.$request->post_id);

        }catch(Exeption $e){
            $this->setErrorMessage($e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //Comment::destroy($id);

        $comment = Comment::find($id); 
        $input = $request->all();
        $input['comment'] = 'This comment was remove by user!';
        $comment->update($input);

        session()->flash('message'.$id,'danger');        
        return redirect('/post/listen/'.$request->post_id.'#'.$id);
    }
}
