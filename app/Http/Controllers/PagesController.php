<?php

namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Page;  
use App\Models\User;

class PagesController extends Controller
{
    //artists
    public function index()
    {
        $pages = Page::all();
        return view('pages.index',compact('pages'));
    }
    public function create()
    {    
        return view('pages.create');
    }
    public function store(Request $request)
    {
        $validated = $request->validate([ 
            'title' => '',
            'content' => '',
            'status' => '', 
            'slug' => '' 
        ]);  
        
        //dd($validated);
        
        //$slug = Str::slug($request->name, '-');
        $slug= $this->createSlug($request->title);
        $input = $request->all();
        $input['slug'] = $slug;  

        //Page::create($input);
        //session()->flash('message','Data insert successfully');
        //return redirect('/dashboard/pages');

        try{
            Page::create($input);
            $this->setSuccessfullyMessage('Data insert successfully.');
            return redirect('/dashboard/pages');
        }catch(Exeption $e){
            $this->setErrorMessage($e->getMessage());
            return redirect()->back();
        }

    }
    // create diffrent slug
    public function createSlug($title, $id = 0)
    {
        $slug = Str::slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Page::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
    // slug---
    public function edit($id)
    {
        $page = Page::find($id); 
        return view('pages.edit',compact('page'));
    }
    public function update(Request $request, $id)
    {         
        $page = Page::find($id);
        $input = $request->all();
        $page->update($input);
        
        session()->flash('message','Data update successfully');
        return redirect('/dashboard/pages');
    }
    public function destroy($id){
        //img distroy
        // $page = Page::find($id);
        // if($Page->audio_path){
        //     unlink("audio/".$Page->audio_path);
        // }else{}

        Page::destroy($id); 

        session()->flash('messageDestroy','Data Delete successfully');
        return redirect('/dashboard/pages');

    }
     

}
