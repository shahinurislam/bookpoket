<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\Artist;
use App\Models\Release;
use App\Models\Comment;
use DB;
use App\Models\User;
use Config\auth;

class PostApiController extends Controller
{    
    public function allposts(){
    //get all post
    //  return DB::table('posts')
    //     ->Join('releases', 'posts.release_id', '=', 'releases.id')
    //     ->Join('artists', 'posts.artist_id', '=', 'artists.id')
    //     ->select('posts.*', 'artists.title as artist_title', 'releases.title as release_title', 'artists.thumbnail_path as artistImg', 'releases.thumbnail_path as releaseImg')
    //     ->get();
    
    // get all id
         return DB::table('posts')
         ->where('posts.status',1) 
         ->select('posts.id')
         ->get(); 
     //->pluck('posts.id');
    
    }
    public function index(){
        
        //   $name = DB::table('posts')
        //     ->Join('releases', 'posts.release_id', '=', 'releases.id')
        //     ->Join('artists', 'posts.artist_id', '=', 'artists.id')   
        //     ->where('posts.status',1)
        //     ->select('posts.*', 'artists.title as artist_title', 'releases.title as release_title', 'artists.thumbnail_path as artistImg', 'releases.thumbnail_path as releaseImg')
        //     ->paginate(5);
        //   return response()->json($name,200); 
           
           
        $name = DB::table('posts')
            ->Join('releases', 'posts.release_id', '=', 'releases.id')
            ->Join('artists', 'posts.artist_id', '=', 'artists.id')   
            ->where('posts.status',1)
            ->select('posts.*', 'artists.title as artist_title', 'releases.title as release_title', 'artists.thumbnail_path as artistImg', 'releases.thumbnail_path as releaseImg',
            DB::raw('(select count(*) from comments where comments.post_id = posts.id) as comment_count'))
            ->inRandomOrder()
            ->paginate(5);
        return response()->json($name,200);
           
           
    }
    public function store()
    {
        $validate = request()->validate([
            'user_id'       => '',
            'artist_id'     => '',
            'release_id'    => '',
            'title'         => '',
            'content'       => '',
            'duration'      => '',
            'audio_path'    => '', 
            'audio_path_url'=> '', 
            'status'        => '', 
            'download_trun' => '', 
            'download_count'=> '',
			'heart_count' => '', 
            'slug'          => ''
        ]);

        return Post::create(
            [
                'user_id'       => request('user_id'),
                'artist_id'     => request('artist_id'),
                'release_id'    => request('release_id'),
                'title'         => request('title'),
                'content'       => request('content'),
                'duration'      => request('duration'),
                'audio_path'    => request('audio_path'),
                'audio_path_url'=> request('audio_path_url'),
                'status'        => request('status'),
                'download_trun' => request('download_trun'),
                'download_count'=> request('download_count'),
                'slug'          => request('slug')
            ]
        );
    }
     public function show($id)
    {
        //return Post::find($id);
        
        return DB::table('posts')
            ->Join('releases', 'posts.release_id', '=', 'releases.id')
            ->Join('artists', 'posts.artist_id', '=', 'artists.id')
            ->where('posts.status',1)
            ->select('posts.*', 'artists.title as artist_title', 'releases.title as release_title', 'artists.thumbnail_path as artistImg', 'releases.thumbnail_path as releaseImg')
            ->where('posts.id', '=', $id)->first();
            //->get();
            
            //$project = Project::join('groups', 'groups.project_id','=', 'project.id')->where('project.id',$id)->first()
       
    }
    public function update(Post $post)
    {
        $validate = request()->validate([
            'user_id'       => '',
            'artist_id'   => '',
            'release_id'         => '',
            'title'       => '',
            'content'       => '',
            'duration' => '',
            'audio_path'        => '', 
            'audio_path_url'        => '', 
            'status'        => '', 
            'download_trun'        => '', 
            'download_count'        => '', 
            'heart_count' => '',
            'slug'        => ''
        ]);

        $success = $post->update($validate);

        return [
            'success' => $success
        ];
    }

    public function destroy(Post $post)
    {
        $success = $post->delete();

        return [
            'success' => $success
        ];
    }


    // public function posts(){
    //     return Post::all();
    // }
    public function artists(){
        return Artist::all();
    }
    public function artistsShow($id)
    {
        return Artist::find($id);
    }
    public function releases(){
        return Release::all();
    }
    
    //
    public function comments(){
        return Comment::all();
    }
    public function commentsStore()
    {
        $validate = request()->validate([
            'post_id'   => '',
            'user_id'   => '',
            'reply_id'  => '',
            'comment'   => '',
        ]);

        return Comment::create(
            [
                'post_id'       => request('post_id'),
                'user_id'     => request('user_id'),
                'reply_id'    => request('reply_id'),
                'comment'         => request('comment')
            ]
        );
    }
    public function commentsShow($id)
    {
        //return DB::table('comments')->where('post_id', $id)->get();

        $name = DB::table('comments')
       ->join('users', 'comments.user_id', '=', 'users.id') 
       ->select('comments.*', 'users.name as user_name')
       ->where('post_id', $id)->paginate(5);
        return response()->json($name,200);

    //    DB::table('comments')
    //    ->select('users.id','users.name','profiles.photo')
    //    ->join('profiles','profiles.id','=','users.id')
    //    ->where(['something' => 'something', 'otherThing' => 'otherThing'])
    //    ->get();

       //return response()->json($commentId);

    }

    //login api
    public function processLogin(Request $request){
        //validation
        $this->validate($request, [
           'email' => 'required|email',
           'password' => 'required|min:6'
       ]);
       $credentials = $request->except(['_token']);
       if (auth()->attempt($credentials)){
            $paste = DB::table('users')->where('email', $request->email)->first(); 
            $paste->invalid = 'Username & Password';
            return response()->json($paste);
       } 
        
       return response()->json(['invalid' => 'Username & Password incorrect']);

   } 
   //register api
   public function prosessRegister(Request $request){
    //validation
    $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|min:6',
        'usr_role' => ''
    ]);
    $data = [
        'name' => $request->input('name'),
        'email' => strtolower($request->input('email')),
        'password' => bcrypt($request->input('password')),
        'usr_role' => strtolower("0"),
    ]; 
    
    $user = User::where('email', '=', $request->input('email'))->first();
    if ($user === null) {
    // user doesn't exist
    User::create($data);
    return response()->json(['invalid' => 'User account created.']);
    }
    return response()->json(['invalid' => 'Email Alredy Exist']);
    
 }

 //search
 public function search($name){

    // $nameSearch = Post::where('title', 'like', "%".$name."%")
    //                 ->orWhere('content', 'like', "%".$name."%")
    //                 ->paginate(5);
    //        return response()->json($nameSearch,200);
    $nameSearch = DB::table('posts')
    ->Join('releases', 'posts.release_id', '=', 'releases.id')
    ->Join('artists', 'posts.artist_id', '=', 'artists.id')   
    ->where('posts.title','like', "%".$name."%")
    ->orWhere('posts.content', 'like', "%".$name."%")
    ->select('posts.*', 'artists.title as artist_title', 'releases.title as release_title', 'artists.thumbnail_path as artistImg', 'releases.thumbnail_path as releaseImg',DB::raw('(select count(*) from comments where comments.post_id = posts.id) as comment_count'))
    ->paginate(5);
   return response()->json($nameSearch,200);

 }
  
}
