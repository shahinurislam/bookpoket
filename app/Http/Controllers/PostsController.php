<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Post;
use App\Models\Artist;
use App\Models\Release;
use App\Models\Comment;
use App\Models\User;

class PostsController extends Controller
{
    //artists
    public function index()
    {
        $posts = Post::all();
        return view('posts.index',compact('posts'));
    }
    public function create()
    {        
        $artists = Artist::all();
        $releases = Release::all();
        return view('posts.create',compact('artists','releases'));
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'user_id' => 'required', 
            'artist_id' => 'required', 
            'release_id' => 'required', 
            'title' => 'required', 
            'content' => '', 
            'duration' => 'required', 
            'audio_path' => 'nullable|file|mimes:audio/mpeg,mpga,mp3,wav,aac',
            //'audio_path' => 'nullable|mimes:application/octet-stream,audio/mpeg,mpga,mp3,wav',
            'audio_path_url' => '',
            'download_trun' => '',
            'download_count' => '',
            'heart_count' => '',
            'slug' => 'unique:posts', 
            'status' => 'required', 
        ]);  
        
        //dd($validated);
        
        //$slug = Str::slug($request->name, '-');
        $slug= $this->createSlug($request->title);
        $input = $request->all();
        $input['slug'] = $slug;

        $input['download_count'] = 0;
        $input['heart_count'] = 0;

        //for audio uploads
        if($request->audio_path == null){  
            $input['audio_path'] = null; 
        }else{              
            $imageName = time().'.'.$request->audio_path->extension();  
            $request->audio_path->move('audio', $imageName);
            // $input = $request->all();
            $input['audio_path'] = $imageName; 
        }

        //Post::create($input);
        //session()->flash('message','Data insert successfully');
        //return redirect('/dashboard/posts');

        try{
            Post::create($input);
            $this->setSuccessfullyMessage('Data insert successfully.');
            return redirect('/dashboard/posts');
        }catch(Exeption $e){
            $this->setErrorMessage($e->getMessage());
            return redirect()->back();
        }

    }
    // create diffrent slug
    public function createSlug($title, $id = 0)
    {
        $slug = Str::slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Post::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
    // slug---

    public function show($id)
    {
        $post = Post::find($id);        
        $comments = Comment::all();     
        $artists = Artist::all();    
        $users = User::all();
        return view('posts.post-single',compact('post','comments','users'));
    }
    public function edit($id)
    {
        $post = Post::find($id);
        $releases = Release::all();
        $artists = Artist::all();
        return view('posts.edit',compact('post','releases','artists'));
    }
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'audio_path' => '',
        ]);
        
        $post = Post::find($id);

        //dd($request->thumbnail_path);
        //img
        if($request->audio_path == null){   
            $input = $request->all();
            $input['audio_path'] = $request->audio_path_re; //replease by value
            $post->update($input);
        }else{
            //if database null
            if($request->audio_path_re == null){
                //for image uploads
                $imageName = time().'.'.$request->audio_path->extension();  
                $request->audio_path->move('audio', $imageName);
                $input = $request->all();
                $input['audio_path'] = "$imageName";
                $post->update($input);
            }else{
                unlink("audio/".$post->audio_path);
                //for image uploads
                $imageName = time().'.'.$request->audio_path->extension();  
                $request->audio_path->move('audio', $imageName);
                $input = $request->all();
                $input['audio_path'] = "$imageName";
                $post->update($input);
            }           
        }
        session()->flash('message','Data update successfully');
        return redirect('/dashboard/posts');
    }
    public function destroy($id){
        //img distroy
        $post = Post::find($id);
        if($post->audio_path){
            unlink("audio/".$post->audio_path);
        }else{}

        Post::destroy($id); 

        session()->flash('messageDestroy','Data Delete successfully');
        return redirect('/dashboard/posts');

    }
    //ajax post updates
    public function updateajax(Request $request, $id)
    {        
         $post = Post::find($id);
         $input = $request->all();            
         $post->update($input);
    }

}
