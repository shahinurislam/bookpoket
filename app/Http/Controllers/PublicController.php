<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Artist;
use App\Models\Release;
use App\Models\Post;
use App\Models\Comment;
use App\Models\User;
use App\Models\Banner;
use App\Models\Page;
use DB;

class PublicController extends Controller
{
    function index() {
        // $releases = Release::orderBy('id','DESC')->get(); 
        // $posts = Post::all();
        // $artists = Artist::all();
        // $banners = Banner::all(); 

        //only for New Release sections
        $releases = Release::orderBy('id','DESC')->paginate(10);
        $rPosts = Post::all();
        $rArtists = Artist::all();


        $posts = Post::paginate(5);
        $artists = Artist::paginate(10);
        $banners = Banner::paginate(12); 
        return view('index',compact('artists','posts','banners','releases','rPosts','rArtists'));
    }
    function artists() {
        $artists = Artist::orderBy('id','DESC')->get();
        return view('artists',compact('artists'));
    }
    public function artistsShow($id)
    {
        $artist = Artist::find($id);
        return view('artists.artist-single',compact('artist'));
    }
    function listen($id) {
        $release = Release::find($id);
        $posts = Post::all();
        $artists = Artist::all();
        return view('listen',compact('release','posts','artists'));
    }

    function releases() {
        $releases = Release::orderBy('id','DESC')->get();        
        $posts = Post::all();
        $artists = Artist::all();
        return view('releases',compact('releases','posts','artists'));
    }     
    function productions() {
        return view('productions');
    } 
    //create comment
    public function listenShow($id)
    {
        $post = Post::find($id);        
        $comments = Comment::all();     
        $artists = Artist::all();    
        $users = User::all();
        return view('posts.post-single',compact('post','comments','users','artists'));
    }    
    public function pageShow($slug)
    {
        $page = Page::where('slug',$slug)->first();
        return view('page',compact('page'));
    }
    //search
    public function search(Request $request){

        //dd($request->content);
        $artists = Artist::all();    
        $users = User::all();
        $search_posts = Post::where('title','LIKE','%'.$request->content.'%')->orWhere('content','LIKE','%'.$request->content.'%')->get(); 

       // dd($search_posts);


        return view('search',compact('search_posts','artists','users'));

    }
    
}
