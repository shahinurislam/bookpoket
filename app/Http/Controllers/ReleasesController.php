<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Release;
use App\Models\Post;

class ReleasesController extends Controller
{
    //artists
    public function index()
    {
        $releases = Release::all();
        return view('releases.index',compact('releases',));
    }
    public function create()
    {
        return view('releases.create');
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'user_id' => 'required', 
            'title' => 'required', 
            'content' => 'required', 
            'thumbnail_path' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
            'slug' => 'unique:posts', 
            'status' => 'required', 
        ]);

        //$slug = Str::slug($request->name, '-');
        $slug= $this->createSlug($request->title);
        $input = $request->all();
        $input['slug'] = "$slug";

        //for image uploads
        $imageName = time().'.'.$request->thumbnail_path->extension();  
        $request->thumbnail_path->move('images/releases/', $imageName);
       // $input = $request->all();
        $input['thumbnail_path'] = "$imageName";  

        Release::create($input);

        session()->flash('message','Data insert successfully');
        return redirect('/dashboard/releases');

    }
    // create diffrent slug
    public function createSlug($title, $id = 0)
    {
        $slug = Str::slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Release::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
    // slug---

    public function show($id)
    {
        $release = Release::find($id);
        return view('releases.release-single',compact('release'));
    }
    public function edit($id)
    {
        $release = Release::find($id);
        return view('releases.edit',compact('release'));
    }
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'thumbnail_path' => '',
        ]);
        
        $release = Release::find($id);

        //dd($request->thumbnail_path);
        //img
        if($request->thumbnail_path == null){   
            $input = $request->all();
            $input['thumbnail_path'] = $request->thumbnail_path_re; //replease by value
            $release->update($input);
        }else{
            unlink("images/releases/".$release->thumbnail_path);
            //for image uploads
            $imageName = time().'.'.$request->thumbnail_path->extension();  
            $request->thumbnail_path->move('images/releases/', $imageName);
            $input = $request->all();
            $input['thumbnail_path'] = "$imageName";
            $release->update($input);
        }
        session()->flash('message','Data update successfully');
        return redirect('/dashboard/releases');
    }
    public function destroy($id)
    {
        //img distroy
        $release = Release::find($id);
        if($release->thumbnail_path){
            unlink("images/releases/".$release->thumbnail_path);
        }else{}

        Release::destroy($id); 

        session()->flash('messageDestroy','Data Delete successfully');
        return redirect('/dashboard/releases');

    }
}
