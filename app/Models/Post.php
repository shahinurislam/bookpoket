<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

   protected $fillable = [
       'user_id',
       'artist_id',
       'release_id',
       'title',
       'content',
       'duration',
       'audio_path',
       'audio_path_url',
       'status',
       'download_trun',
       'download_count',
       'heart_count',
       'slug',
    ];
}