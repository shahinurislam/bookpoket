-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 18, 2023 at 09:07 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookpocket`
--

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `thumbnail_path` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `user_id`, `title`, `content`, `thumbnail_path`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(2, 0, 'মিজানুর রহমান আজহারী', 'মিজানুর রহমান আজহারী (২৬ জানুয়ারি ১৯৯০) একজন বাংলাদেশি ইসলামি বক্তা। ঢাকা জেলার ডেমরায় তিনি জন্মগ্রহণ করেন। ইসলামি আলোচক হিসেবে তিনি জনপ্রিয় এবং একইসাথে সমালোচিত। বিভিন্ন বক্তব্যে তিনি নিজেকে মধ্যমপন্থী ইসলামী আলোচক বলে দাবি করেন।', '1683987855.jpg', '1', 'mijanur-rhman-ajharee', '2023-05-13 08:24:15', '2023-05-13 08:24:15'),
(3, 0, 'শায়খ আহমাদুল্লাহ', 'শায়খ আহমাদুল্লাহ বাংলাদেশের স্বনামধন্য ইসলামী ব্যক্তিত্ব। বিদগ্ধ আলোচক, লেখক ও খতীব। ইসলামের খেদমতে তিনি নানামুখী কাজ করেন। লেখালেখি, গবেষণা ও সভা-সেমিনারে লেকচারসহ নানামুখী দাওয়াতি কার্যক্রম পরিচালনা করেন। উন্মুক্ত ইসলামিক প্রোগ্রাম ও প্রশ্নোত্তরমূলক অনুষ্ঠানে অংশগ্রহণ এবং টিভি অনুষ্ঠানে সময় দেওয়াসহ বহুমুখী সেবামূলক কাজে সপ্রতিভ গুণী ও স্বনামধন্য এই আলেমে দীন। দেশে-বিদেশে শিক্ষা, সেবা ও দাওয়াহ— ছড়িয়ে দিতে শায়খ আহমাদুল্লাহ প্রতিষ্ঠা করেছেন ‘আস-সুন্নাহ ফাউন্ডেশন’।', '1683988120.jpg', '1', 'saykh-ahmadullah', '2023-05-13 08:28:40', '2023-05-13 08:28:40'),
(4, 0, 'আবু ত্বহা মুহাম্মদ আদনান', 'আবু ত্ব-হা মুহাম্মদ আদনান নামেই সবার কাছে পরিচিত। ৩১ বছর বয়সী এই তরুণ বক্তার বাড়ি রাজশাহী হলেও তিনি রংপুর জেলায় নানা বাড়িতে বড় হয়েছেন। ২০১৮ সালে আলোকিত জ্ঞানী প্রতিযোগিতায় প্রথম রানার আর্প হয়েছিলেন তিনি।', '1683988516.jpg', '1', 'abu-twha-muhammd-adnan', '2023-05-13 08:35:16', '2023-05-13 08:35:16'),
(5, 0, 'দেলাওয়ার হোসাইন সাঈদী', 'দেলাওয়ার হোসাইন সাঈদী, একজন বাংলাদেশী ইসলামী পণ্ডিত, বক্তা এবং রাজনীতিবিদ ও প্রাক্তন সংসদ সদস্য; তিনি বাংলাদেশের রাজনৈতিক দল বাংলাদেশ জামায়াতে ইসলামীর নায়েবে-আমির বা ভাইস প্রেসিডেন্ট। তিনি ১৯৯৬ সালের সাধারণ নির্বাচনে জয়ী হয়ে প্রথমবার এবং ২০০১ সালের সাধারণ নির্বাচনে জয়ী হয়ে তিনি দ্বিতীয়বারের মতো বাংলাদেশ জাতীয় সংসদের সংসদ সদস্য নির্বাচিত হয়েছিলেন।', '1683989154.jpg', '1', 'delaoozar-hosain-saeedee', '2023-05-13 08:45:54', '2023-05-13 08:45:54'),
(6, 0, 'আল্লামা তারেক মনোয়ার', 'আল্লামা তারেক মনোয়ার বর্তমান সময়ে বাংলাদেশের আলোচিত ও জনপ্রিয় একজন ইসলাম ধর্ম প্রচারক যাকে ঘিরে বাংলাদেশসহ বিশ্বের বিভিন্ন দেশে অসংখ্য ওয়াজ মাহফিল অনুষ্ঠিত হয়েছে বা এখন হচ্ছে যেখানে শিশু-কিশোর নবীন-প্রবীণ সংঘাতে লাখো মানুষের মিলনমেলা। কেবলমাত্র উপমহাদেশসহ বিশ্বজুড়ে ইসলামের নামে অপপ্রচার ধর্মের নামে সন্ত্রাসবাদের উত্থানের বিরুদ্ধে যেসব ইসলামিক স্কলার সোচ্চার আওয়াজ তুলেছেন তাদের মধ্যে অন্যতম মাওলানা তারেক মনোয়ার।', '1683989512.jpg', '1', 'allama-tarek-mnozar', '2023-05-13 08:51:52', '2023-05-13 08:51:52'),
(7, 0, 'শায়খ আব্দুর রাজ্জাক বিন ইউসুফ', 'কোরআন সুন্নাহ নিয়ে বক্তব্য, ইসলামিক বই লেখনী, শিক্ষকতা, পত্রিকা সম্পাদনা ও মাদ্রাসা সহ বিভিন্ন প্রতিষ্ঠান পরিচালনা করেন।', '1683989704.jpg', '1', 'sazkh-abdur-rajjak-bin-iusuf', '2023-05-13 08:55:04', '2023-05-13 08:55:04');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `thumbnail_path` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `user_id`, `title`, `content`, `thumbnail_path`, `status`, `link`, `slug`, `created_at`, `updated_at`) VALUES
(3, 0, 'ঈদ উল আযহা', 'ঈদ উল আযহা বা ঈদ উল আজহা বা ঈদ উল আধহা (আরবি: عيد الأضحى ....', '1683987500.png', '1', 'https://bn.wikipedia.org/wiki/%E0%A6%88%E0%A6%A6%E0%A7%81%E0%A6%B2_%E0%A6%86%E0%A6%AF%E0%A6%B9%E0%A6%BE', 'ramadan', '2023-05-13 08:12:16', '2023-05-13 08:18:20');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reply_id` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_12_20_094242_create_artists_table', 1),
(6, '2021_12_24_035740_create_releases_table', 1),
(7, '2021_12_24_150934_create_posts_table', 1),
(8, '2021_12_30_084940_create_comments_table', 1),
(9, '2022_03_20_101334_create_banners_table', 1),
(10, '2023_01_29_083147_create_pages_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `release_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `duration` longtext DEFAULT NULL,
  `audio_path` varchar(255) DEFAULT NULL,
  `audio_path_url` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `download_trun` varchar(255) DEFAULT NULL,
  `download_count` varchar(255) DEFAULT NULL,
  `heart_count` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `artist_id`, `release_id`, `title`, `content`, `duration`, `audio_path`, `audio_path_url`, `status`, `download_trun`, `download_count`, `heart_count`, `slug`, `created_at`, `updated_at`) VALUES
(3, 0, 2, 2, 'রাসুলের প্রেমে গজল গাইলেন আজহারী', 'রাসুলের প্রেমে গজল গাইলেন আজহারী', '5.60', NULL, NULL, '1', '1', '1', '0', 'rasuler-preme-gjl-gailen-ajharee', '2023-05-13 09:06:31', '2023-05-13 09:06:54');

-- --------------------------------------------------------

--
-- Table structure for table `releases`
--

CREATE TABLE `releases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `thumbnail_path` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `releases`
--

INSERT INTO `releases` (`id`, `user_id`, `title`, `content`, `thumbnail_path`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(2, 0, 'বাংলা নাশিদ', 'নাশিদ (আরবি: একবচন نشيد nashīd, বহুবচন أناشيد anāshīd, বঙ্গানুবাদে: \"গান\") সঙ্গীত এর একটি ধারা‌। ইসলামের একটি নির্দিষ্ট শৈলী বা ঐতিহ্য অনুসারে গানগুলো গাওয়া হয় খালি গলায় অথবা যন্ত্রের সাথে। নাশিদ সমগ্র ইসলামী বিশ্ব জুড়ে জনপ্রিয়। একটি নাশিদের উপাদানে বা গানে সাধারণত ইসলামী বিশ্বাস, ইতিহাস এবং ধর্মের পাশাপাশি বর্তমান ঘটনাগুলি উল্লেখ করা হয়।[', '1683990252.jpg', '1', 'islamik-nasid', '2023-05-13 08:59:42', '2023-05-13 09:04:12'),
(3, 0, 'ওয়াজ মাহফিল', 'ওয়াজ মাহফিল হলো উপমহাদেশের মুসলিমদের ইসলামি নসিহত, উপদেশ, জ্ঞান বিতরণ ও ইসলামি পরামর্শ প্রদান করার পাবলিক স্থান, যেখানে একজন ইসলামি বক্তা বা ওয়ায়েজিন কিছু সময় ধরে বহু মানুষের সামনে বক্তৃতা দিয়ে থাকেন। এটি উপমহাদেশে বিশেষ করে বাংলাদেশে বহুল প্রচলিত ইসলাম প্রচারের মাধ্যম। যদিও ওয়াজ মাহফিলের মাধ্যমে ইসলাম প্রচার ছাড়াও দেশের নানা রাজনৈতিক, সামাজিক ও সমসাময়িক বিষয় নিয়েও আলোচনা হয়ে থাকে।', '1683990233.jpg', '1', 'waj-mahfil', '2023-05-13 09:03:53', '2023-05-13 09:03:53'),
(4, 0, 'কলরব', 'Kalarab is an Islamic nasheed ensemble from Bangladesh consisting of thirteen members. Presently, the group has amassed over 4 million subscribers and 760 million total views on the YouTube channel of their main associated record label, Holy Tune.', '1684141547.jpg', '1', 'klrb', '2023-05-15 03:05:47', '2023-05-15 03:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `usr_role` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `usr_role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Md Shahinur Islam', 'shahin@gmail.com', NULL, '$2y$10$SpOKp5UEwTKj/XnkFdy7Je7/cyy/JRQ.hVYU4IlJrF2HfenPznSmW', '1', NULL, '2023-05-13 04:26:33', '2023-05-13 04:26:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `releases`
--
ALTER TABLE `releases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artists`
--
ALTER TABLE `artists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `releases`
--
ALTER TABLE `releases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
