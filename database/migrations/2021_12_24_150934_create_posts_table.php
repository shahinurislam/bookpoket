<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('artist_id');
            $table->integer('release_id');
            $table->string('title')->nullable();
            $table->longtext('content')->nullable();
            $table->longtext('duration')->nullable();
            $table->string('audio_path')->nullable();
            $table->string('audio_path_url')->nullable();
            $table->string('status')->nullable();
            $table->string('download_trun')->nullable();
            $table->string('download_count')->nullable();
            $table->string('heart_count')->nullable();
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
