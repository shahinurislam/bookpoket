@extends('master')
@section('content')

	<!-- main content -->
	<main class="main">
		<div class="container-fluid">
			<!-- artists -->
			<div class="row row--grid">
				<!-- breadcrumb -->
				<div class="col-12">
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">Artists</li>
					</ul>
				</div>
				<!-- end breadcrumb -->

                <div class="row row--grid">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <form action="{{ url('/dashboard/artist') }}/{{ $artist->id }}" method="post" enctype="multipart/form-data" class="sign__form sign__form--contacts">
                        {{ csrf_field() }}
                        @method('PATCH')
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="sign__group">
                                        <input type="text" name="title" class="sign__input" placeholder="Name" value="{{ $artist->title }}"/>
                                        <input type="hidden" name="user_id" class="sign__input" value="0" placeholder="user id">
                                    </div>
                                </div>                                

                                <div class="col-12">
                                    <div class="sign__group">
                                        <textarea name="content" class="sign__textarea" placeholder="Content" value='{{ $artist->content }}'>{{ $artist->content }}</textarea>
                                    </div>
                                </div>

                                <div class="col-12 col-md-11">
                                    <div class="sign__group">
                                        <input type="hidden" name="thumbnail_path_re" class="sign__input img-p"  value="{{ $artist->thumbnail_path }}" />
                                        <input type="file" name="thumbnail_path" class="sign__input img-p" accept="image/*">
                                    </div>
                                </div>
                                <div class="col-12 col-md-1">
                                    <div class="sign__group">
                                        <img src="{{ asset('images/artists') }}/{{$artist->thumbnail_path }}" width="50px" height="50px" class="border border-info">

                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="sign__group"> 
                                        <select name="status" class="sign__input" placeholder="status">
                                            <option value="1" {{ $artist->status == 1 ? 'selected' : '' }} >Active</option>
                                            <option value="0" {{ $artist->status == 0 ? 'selected' : '' }} >Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-xl-12">
                                    <button type="submit" class="sign__btn">Save</button>
                                </div>
                            </div>
                        </form>	
                    </div>                  
                </div>                
			</div>
		</div>
	</main>
	<!-- end main content -->
@endsection