@extends('master')
@section('content')

	<!-- main content -->
	<main class="main">
		<div class="container-fluid">
			<!-- artists -->
			<div class="row row--grid">
				<!-- breadcrumb -->
				<div class="col-12">
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">Artists</li>
					</ul>
				</div>
				<!-- end breadcrumb -->

				<!-- title -->
				<!-- <div class="col-12">
                    <div class="row">
                        <div class="container">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Thumb</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td>Delete</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Mary</td>
                                    <td>Moe</td>
                                    <td>mary@example.com</td>
                                    <td>Delete</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>July</td>
                                    <td>Dooley</td>
                                    <td>july@example.com</td>
                                    <td>Delete</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>                        
                    </div>
				</div> -->
				<!-- end title -->
                <div class="row row--grid">
				    <div class="col-12">
                        <div class="profile">
                            <!-- tabs nav -->
                            <ul class="nav nav-tabs profile__tabs" id="profile__tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">Artists</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/dashboard/artist/create') }}">Add New</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" >
                                    @if(session()->has('message'))
                                    <br/>
                                    <div class="alert alert-success" role="alert">
                                        {{session('message')}}
                                    </div>
                                    @endif
                                    @if(session()->has('messageDestroy'))
                                    <br/>
                                    <div class="alert alert-danger" role="alert">
                                        {{session('messageDestroy')}}
                                    </div>
                                    @endif
                                    </a>
                                </li>
                            </ul>
                            <!-- end tabs nav -->
                        </div>
                        <!-- content tabs -->
                        <div class="tab-content">
                            <div class="tab-pane fade  show active" id="tab-1" role="tabpanel">
                                <div class="row row--grid">
                                    <div class="col-12">
                                        <div class="dashbox">
                                            <div class="dashbox__table-wrap">
                                                <div class="dashbox__table-scroll">
                                                    <table class="main__table" id="myTable">
                                                        <thead>
                                                            <tr>
                                                                <th>№</th>
                                                                <th><a href="#">Artists <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9.71,10.21,12,7.91l2.29,2.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42l-3-3a1,1,0,0,0-1.42,0l-3,3a1,1,0,0,0,1.42,1.42Zm4.58,4.58L12,17.09l-2.29-2.3a1,1,0,0,0-1.42,1.42l3,3a1,1,0,0,0,1.42,0l3-3a1,1,0,0,0-1.42-1.42Z"/></svg></a></th>
                                                                <th><a href="#" class="active">Name <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,13.41,12.71,9.17a1,1,0,0,0-1.42,0L7.05,13.41a1,1,0,0,0,0,1.42,1,1,0,0,0,1.41,0L12,11.29l3.54,3.54a1,1,0,0,0,.7.29,1,1,0,0,0,.71-.29A1,1,0,0,0,17,13.41Z"/></svg></a></th>
                                                                <th><a href="#" class="active">Date <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,9.17a1,1,0,0,0-1.41,0L12,12.71,8.46,9.17a1,1,0,0,0-1.41,0,1,1,0,0,0,0,1.42l4.24,4.24a1,1,0,0,0,1.42,0L17,10.59A1,1,0,0,0,17,9.17Z"/></svg></a></th>
                                                               
                                                                <th><a href="#">Status <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9.71,10.21,12,7.91l2.29,2.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42l-3-3a1,1,0,0,0-1.42,0l-3,3a1,1,0,0,0,1.42,1.42Zm4.58,4.58L12,17.09l-2.29-2.3a1,1,0,0,0-1.42,1.42l3,3a1,1,0,0,0,1.42,0l3-3a1,1,0,0,0-1.42-1.42Z"/></svg></a></th>
                                                                <th><a href="#">Action <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9.71,10.21,12,7.91l2.29,2.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42l-3-3a1,1,0,0,0-1.42,0l-3,3a1,1,0,0,0,1.42,1.42Zm4.58,4.58L12,17.09l-2.29-2.3a1,1,0,0,0-1.42,1.42l3,3a1,1,0,0,0,1.42,0l3-3a1,1,0,0,0-1.42-1.42Z"/></svg></a></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @php
                                                        $sl = 0;
                                                        @endphp
                                                        @foreach($artists as $artist)
                                                            <tr>
                                                                <td>
                                                                    <div class="main__table-text main__table-text--number"><a href="#modal-info" class="open-modal">{{ ++$sl }}</a></div>
                                                                </td>
                                                                <td>
                                                                    <div class="main__table-img">
                                                                    <img src="{{ asset('images/artists') }}/{{$artist->thumbnail_path }}"/>
                                                                    </div>
                                                                </td>	
                                                                <td>
                                                                    <div class="main__table-text"><a href="#">{{ $artist->title }}</a></div>
                                                                </td>
                                                                <td>
                                                                    <div class="main__table-text">{{ $artist->created_at }}</div>
                                                                </td>
                                                                <td>
                                                                    @if($artist->status == 0)
                                                                        <div class="main__table-text main__table-text--red"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15.71,8.29a1,1,0,0,0-1.42,0L12,10.59,9.71,8.29A1,1,0,0,0,8.29,9.71L10.59,12l-2.3,2.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0L12,13.41l2.29,2.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L13.41,12l2.3-2.29A1,1,0,0,0,15.71,8.29Zm3.36-3.36A10,10,0,1,0,4.93,19.07,10,10,0,1,0,19.07,4.93ZM17.66,17.66A8,8,0,1,1,20,12,7.95,7.95,0,0,1,17.66,17.66Z"/></svg> Inactive</div>
                                                                    @else
                                                                        <div class="main__table-text main__table-text--green"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M14.72,8.79l-4.29,4.3L8.78,11.44a1,1,0,1,0-1.41,1.41l2.35,2.36a1,1,0,0,0,.71.29,1,1,0,0,0,.7-.29l5-5a1,1,0,0,0,0-1.42A1,1,0,0,0,14.72,8.79ZM12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Z"/></svg>  Active</div>
                                                                    
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <div class="main__table-text">
                                                                        <a href="{{ url('dashboard/artist/'.$artist->id) }}">View</a>|<a href="{{ url('dashboard/artist/'.$artist->id.'/edit') }}">Edit</a>|

                                                                        <form action="{{ url('dashboard/artist') }}/{{ $artist->id }}" method="POST">
                                                                        {{ csrf_field() }}
                                                                        @method('DELETE')
                                                                            <input type="submit" value="Delete"/>
                                                                        </form>
                                                                    </div> 
                                                                </td>
                                                            </tr>

                                                            @endforeach               
                                                           
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end content tabs -->
				    </div>
			    </div>	
			</div>
		</div>
	</main>
	<!-- end main content -->
@endsection
