@extends('master')
@section('content')
<main class="main">
		<div class="container-fluid">
			<!-- slider -->
			<section class="row">
				<div class="col-12">
					<div class="hero owl-carousel" id="hero">
						@foreach($banners as $banner)
						<div class="hero__slide" data-bg="{{ asset('images/banners') }}/{{$banner->thumbnail_path }}">
							<h1 class="hero__title">{{ $banner->title }}</h1>
							<p class="hero__text">{{ $banner->content }}</p>
							<div class="hero__btns">
								<a href="{{$banner->link}}" class="hero__btn hero__btn--red">Learn More</a>
								<a href="{{$banner->link}}" class="hero__btn hero__btn--video open-video"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M16,10.27,11,7.38A2,2,0,0,0,8,9.11v5.78a2,2,0,0,0,1,1.73,2,2,0,0,0,2,0l5-2.89a2,2,0,0,0,0-3.46ZM15,12l-5,2.89V9.11L15,12ZM12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Z"/></svg> Now Play</a>
							</div>
						</div>
						@endforeach
						
					</div>

					<button class="main__nav main__nav--hero main__nav--prev" data-nav="#hero" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,11H9.41l3.3-3.29a1,1,0,1,0-1.42-1.42l-5,5a1,1,0,0,0-.21.33,1,1,0,0,0,0,.76,1,1,0,0,0,.21.33l5,5a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L9.41,13H17a1,1,0,0,0,0-2Z"/></svg></button>
					<button class="main__nav main__nav--hero main__nav--next" data-nav="#hero" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></button>
				</div>
			</section>
			<!-- end slider -->

			<!-- releases -->
			<section class="row row--grid">
				<!-- title -->
				<div class="col-12">
					<div class="main__title">
						<h2>New Releases</h2>

						<a href="{{ url('/releases') }}" class="main__link">See all <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></a>
					</div>
				</div>
				<!-- end title -->
				@foreach($releases as $release)
					@if($release->status == 1)
				<div class="col-6 col-sm-4 col-lg-2">
					<div class="album">
						<div class="album__cover">
							<img src="{{ asset('images/releases') }}/{{$release->thumbnail_path }}" alt="">
							<a href="{{ url('listen/'.$release->id) }}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"/></svg></a>
							<span class="album__stat">
								<span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M3.71,16.29a1,1,0,0,0-.33-.21,1,1,0,0,0-.76,0,1,1,0,0,0-.33.21,1,1,0,0,0-.21.33,1,1,0,0,0,.21,1.09,1.15,1.15,0,0,0,.33.21.94.94,0,0,0,.76,0,1.15,1.15,0,0,0,.33-.21,1,1,0,0,0,.21-1.09A1,1,0,0,0,3.71,16.29ZM7,8H21a1,1,0,0,0,0-2H7A1,1,0,0,0,7,8ZM3.71,11.29a1,1,0,0,0-1.09-.21,1.15,1.15,0,0,0-.33.21,1,1,0,0,0-.21.33.94.94,0,0,0,0,.76,1.15,1.15,0,0,0,.21.33,1.15,1.15,0,0,0,.33.21.94.94,0,0,0,.76,0,1.15,1.15,0,0,0,.33-.21,1.15,1.15,0,0,0,.21-.33.94.94,0,0,0,0-.76A1,1,0,0,0,3.71,11.29ZM21,11H7a1,1,0,0,0,0,2H21a1,1,0,0,0,0-2ZM3.71,6.29a1,1,0,0,0-.33-.21,1,1,0,0,0-1.09.21,1.15,1.15,0,0,0-.21.33.94.94,0,0,0,0,.76,1.15,1.15,0,0,0,.21.33,1.15,1.15,0,0,0,.33.21,1,1,0,0,0,1.09-.21,1.15,1.15,0,0,0,.21-.33.94.94,0,0,0,0-.76A1.15,1.15,0,0,0,3.71,6.29ZM21,16H7a1,1,0,0,0,0,2H21a1,1,0,0,0,0-2Z"/></svg>
							<!-- count audio on this release -->
							@php $audioCount = 0; @endphp
								@foreach($rPosts as $post)
									@if($post->release_id == $release->id && $post->status == 1)
										@php $audioCount+= 1; @endphp
									@endif
								@endforeach 
							{{ $audioCount }}
							</span>
								<span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20,13.18V11A8,8,0,0,0,4,11v2.18A3,3,0,0,0,2,16v2a3,3,0,0,0,3,3H8a1,1,0,0,0,1-1V14a1,1,0,0,0-1-1H6V11a6,6,0,0,1,12,0v2H16a1,1,0,0,0-1,1v6a1,1,0,0,0,1,1h3a3,3,0,0,0,3-3V16A3,3,0,0,0,20,13.18ZM7,15v4H5a1,1,0,0,1-1-1V16a1,1,0,0,1,1-1Zm13,3a1,1,0,0,1-1,1H17V15h2a1,1,0,0,1,1,1Z"/></svg> 0</span>
							</span>
						</div>
						<div class="album__title">
						<h3><a href="{{ url('listen/'.$release->id) }}">{{ $release->title }}</a></h3>
							<span>
							<!-- artist name show -->
								@foreach($rPosts as $post)
									@if($post->release_id == $release->id && $post->status == 1)	 
										@foreach($rArtists as $artist)
											@if($post->artist_id == $artist->id)			
											<!-- <a href="{{ url('artist/'.$artist->id) }}">{{ $artist->title }}</a>  -->
											@endif
										@endforeach
									@endif
								@endforeach
							</span>
						</div>
					</div>
				</div>
					@endif
				@endforeach				
			</section>
			
			<!-- end releases -->

			<!-- events -->
			<!-- <section class="row row--grid">
				
				<div class="col-12">
					<div class="main__title">
						<h2>Upcoming Events</h2>

						<a href="events.html" class="main__link">See all <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></a>
					</div>
				</div>
			

				<div class="col-12">
					<div class="main__carousel-wrap">
						<div class="main__carousel main__carousel--events owl-carousel" id="events">
							<div class="event" data-bg="{{ asset('assets/img/events/event1.jpg')}}">
								<a href="#modal-ticket" class="event__ticket open-modal"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9,10a1,1,0,0,0-1,1v2a1,1,0,0,0,2,0V11A1,1,0,0,0,9,10Zm12,1a1,1,0,0,0,1-1V6a1,1,0,0,0-1-1H3A1,1,0,0,0,2,6v4a1,1,0,0,0,1,1,1,1,0,0,1,0,2,1,1,0,0,0-1,1v4a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V14a1,1,0,0,0-1-1,1,1,0,0,1,0-2ZM20,9.18a3,3,0,0,0,0,5.64V17H10a1,1,0,0,0-2,0H4V14.82A3,3,0,0,0,4,9.18V7H8a1,1,0,0,0,2,0H20Z"/></svg> Buy ticket</a>
								<span class="event__date">March 14, 2021</span>
								<span class="event__time">8:00 pm</span>
								<h3 class="event__title"><a href="event.html">Sorry Babushka</a></h3>
								<p class="event__address">1 East Plumb Branch St.Saint Petersburg, FL 33702</p>
							</div>

							<div class="event" data-bg="{{ asset('assets/img/events/event2.jpg')}}">
								<a href="#modal-ticket" class="event__ticket open-modal"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9,10a1,1,0,0,0-1,1v2a1,1,0,0,0,2,0V11A1,1,0,0,0,9,10Zm12,1a1,1,0,0,0,1-1V6a1,1,0,0,0-1-1H3A1,1,0,0,0,2,6v4a1,1,0,0,0,1,1,1,1,0,0,1,0,2,1,1,0,0,0-1,1v4a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V14a1,1,0,0,0-1-1,1,1,0,0,1,0-2ZM20,9.18a3,3,0,0,0,0,5.64V17H10a1,1,0,0,0-2,0H4V14.82A3,3,0,0,0,4,9.18V7H8a1,1,0,0,0,2,0H20Z"/></svg> Buy ticket</a>
								<span class="event__date">March 16, 2021</span>
								<span class="event__time">7:00 pm</span>
								<h3 class="event__title"><a href="event.html">Big Daddy</a></h3>
								<p class="event__address">71 Pilgrim Avenue Chevy Chase, MD 20815</p>
							</div>

							<div class="event" data-bg="{{ asset('assets/img/events/event3.jpg')}}">
								<span class="event__out">Sold out</span>
								<span class="event__date">March 23, 2021</span>
								<span class="event__time">9:30 pm</span>
								<h3 class="event__title"><a href="event.html">Rocky Pub</a></h3>
								<p class="event__address">514 S. Magnolia St. Orlando, FL 32806</p>
							</div>

							<div class="event" data-bg="{{ asset('assets/img/events/event4.jpg')}}">
								<a href="#modal-ticket" class="event__ticket open-modal"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9,10a1,1,0,0,0-1,1v2a1,1,0,0,0,2,0V11A1,1,0,0,0,9,10Zm12,1a1,1,0,0,0,1-1V6a1,1,0,0,0-1-1H3A1,1,0,0,0,2,6v4a1,1,0,0,0,1,1,1,1,0,0,1,0,2,1,1,0,0,0-1,1v4a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V14a1,1,0,0,0-1-1,1,1,0,0,1,0-2ZM20,9.18a3,3,0,0,0,0,5.64V17H10a1,1,0,0,0-2,0H4V14.82A3,3,0,0,0,4,9.18V7H8a1,1,0,0,0,2,0H20Z"/></svg> Buy ticket</a>
								<span class="event__date">March 30, 2021</span>
								<span class="event__time">6:00 pm</span>
								<h3 class="event__title"><a href="event.html">Big Club</a></h3>
								<p class="event__address">123 6th St. Melbourne, FL 32904</p>
							</div>

							<div class="event" data-bg="{{ asset('assets/img/events/event5.jpg')}}">
								<span class="event__out">Sold out</span>
								<span class="event__date">March 30, 2021</span>
								<span class="event__time">10:00 pm</span>
								<h3 class="event__title"><a href="event.html">Big Daddy</a></h3>
								<p class="event__address">71 Pilgrim Avenue Chevy Chase, MD 20815</p>
							</div>

							<div class="event" data-bg="{{ asset('assets/img/events/event6.jpg')}}">
								<a href="#modal-ticket" class="event__ticket open-modal"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9,10a1,1,0,0,0-1,1v2a1,1,0,0,0,2,0V11A1,1,0,0,0,9,10Zm12,1a1,1,0,0,0,1-1V6a1,1,0,0,0-1-1H3A1,1,0,0,0,2,6v4a1,1,0,0,0,1,1,1,1,0,0,1,0,2,1,1,0,0,0-1,1v4a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V14a1,1,0,0,0-1-1,1,1,0,0,1,0-2ZM20,9.18a3,3,0,0,0,0,5.64V17H10a1,1,0,0,0-2,0H4V14.82A3,3,0,0,0,4,9.18V7H8a1,1,0,0,0,2,0H20Z"/></svg> Buy ticket</a>
								<span class="event__date">March 31, 2021</span>
								<span class="event__time">6:30 pm</span>
								<h3 class="event__title"><a href="event.html">Rocky Pub</a></h3>
								<p class="event__address">514 S. Magnolia St. Orlando, FL 32806</p>
							</div>
						</div>

						<button class="main__nav main__nav--prev" data-nav="#events" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,11H9.41l3.3-3.29a1,1,0,1,0-1.42-1.42l-5,5a1,1,0,0,0-.21.33,1,1,0,0,0,0,.76,1,1,0,0,0,.21.33l5,5a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L9.41,13H17a1,1,0,0,0,0-2Z"/></svg></button>
						<button class="main__nav main__nav--next" data-nav="#events" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></button>
					</div>
				</div>
			</section> -->
			<!-- end events -->

			<!-- articts -->
			<section class="row row--grid">
				<!-- title -->
				<div class="col-12">
					<div class="main__title">
						<h2>Artists</h2>

						<a href="{{ url('/artists') }}" class="main__link">See all <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></a>
					</div>
				</div>
				<!-- end title -->

				<div class="col-12">
					<div class="main__carousel-wrap">
						<div class="main__carousel main__carousel--artists owl-carousel" id="artists">
						@foreach($artists as $artist)
						@if($artist->status == 1)
							<a href="{{ url('artist/'.$artist->id) }}" class="artist">
								<div class="artist__cover">
									<img src="{{ asset('images/artists') }}/{{$artist->thumbnail_path }}" alt="">
								</div>
								<h3 class="artist__title">{{ $artist->title }}</h3>
							</a>
						@endif
						@endforeach
							
						</div>

						<button class="main__nav main__nav--prev" data-nav="#artists" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,11H9.41l3.3-3.29a1,1,0,1,0-1.42-1.42l-5,5a1,1,0,0,0-.21.33,1,1,0,0,0,0,.76,1,1,0,0,0,.21.33l5,5a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L9.41,13H17a1,1,0,0,0,0-2Z"/></svg></button>
						<button class="main__nav main__nav--next" data-nav="#artists" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></button>
					</div>
				</div>
			</section>
			<!-- end articts -->

			<section class="row row--grid">
				<div class="col-12 col-md-6 col-xl-4">
					<div class="row row--grid">
						<!-- title -->
						<div class="col-12">
							<div class="main__title">
								<h2><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M21.65,2.24a1,1,0,0,0-.8-.23l-13,2A1,1,0,0,0,7,5V15.35A3.45,3.45,0,0,0,5.5,15,3.5,3.5,0,1,0,9,18.5V10.86L20,9.17v4.18A3.45,3.45,0,0,0,18.5,13,3.5,3.5,0,1,0,22,16.5V3A1,1,0,0,0,21.65,2.24ZM5.5,20A1.5,1.5,0,1,1,7,18.5,1.5,1.5,0,0,1,5.5,20Zm13-2A1.5,1.5,0,1,1,20,16.5,1.5,1.5,0,0,1,18.5,18ZM20,7.14,9,8.83v-3L20,4.17Z"/></svg><a href="#">Top Singles</a></h2>
							</div>
						</div>
						<!-- end title -->

						<div class="col-12">
							<ul class="main__list">
							@php $count = 1; @endphp
							@foreach($posts as $post)
								@if($post->status == 1)
								<li class="single-item">
									<span class="single-item__number">{{ $count }}</span>
									<!-- count -->
									@if($loop->iteration % 2 == 0)										
										<span class="single-item__rate"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12.71,12.54a1,1,0,0,0-1.42,0l-3,3A1,1,0,0,0,9.71,17L12,14.66,14.29,17a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42Zm-3-1.08L12,9.16l2.29,2.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42l-3-3a1,1,0,0,0-1.42,0l-3,3a1,1,0,0,0,1.42,1.42Z"/></svg> {{ $count }}</span>
									@else
										<span class="single-item__rate single-item__rate--red"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.29,11.46a1,1,0,0,0,1.42,0l3-3A1,1,0,1,0,14.29,7L12,9.34,9.71,7A1,1,0,1,0,8.29,8.46Zm3,1.08L12,14.84l-2.29-2.3A1,1,0,0,0,8.29,14l3,3a1,1,0,0,0,1.42,0l3-3a1,1,0,0,0-1.42-1.42Z"/></svg> {{ $count }}</span>
									@endif
									<!-- count -->
									<a data-link data-title="{{ $post->title}}"

									@foreach($artists as $artist)
										@if($artist->id == $post->artist_id)
											data-artist="{{ $artist->title }}"                                      
											data-img="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}"                                   
										@endif
									@endforeach	
									
									href="{{ $post->audio_path_url == null ? asset('audio').'/'.$post->audio_path : $post->audio_path_url }}" class="update{{$post->id}} single-item__cover">

									@foreach($artists as $artist)
										@if($artist->id == $post->artist_id)
                                        <img src="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}" alt="sdsd">
                                    	@endif
									@endforeach	
										<!-- <img src="{{ asset('assets/img/covers/cover1.jpg')}}" alt=""> -->
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"/></svg>
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M16,2a3,3,0,0,0-3,3V19a3,3,0,0,0,6,0V5A3,3,0,0,0,16,2Zm1,17a1,1,0,0,1-2,0V5a1,1,0,0,1,2,0ZM8,2A3,3,0,0,0,5,5V19a3,3,0,0,0,6,0V5A3,3,0,0,0,8,2ZM9,19a1,1,0,0,1-2,0V5A1,1,0,0,1,9,5Z"/></svg>
									</a>
									<!-- how many times are play  -->
									<script type='text/javascript'>
									var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
										$(document).on("click", ".update{{$post->id}}" , function() {
											var edit_id = {{ $post->id}};

											var name = {{ $post->download_count}} + 1;
												$.ajax({
												url: '/dashboard/post/{{ $post->id}}',
												type: 'patch',
												data: {_token: CSRF_TOKEN,id: edit_id,download_count: name},
												success: function(response){
													alert(response);
												}
												});											 
											}); 
										</script>
									<!-- how many times are play  -->
									
									<div class="single-item__title">
										<h4><a href="{{ url('post/listen/'.$post->id) }}">{{ substr($post->title, 0, 20)}}</a></h4>
										<span>
										@foreach($artists as $artist)
											@if($artist->id == $post->artist_id)
											<a href="{{ url('artist/'.$artist->id) }}">
												{{ $artist->title }}
											</a>
											@endif
										@endforeach 
										</span>
									</div>
									<span class="single-item__time">{{ $post->duration}}</span>
								</li>
								@php $count+= 1; @endphp
								@endif
							@endforeach 
								
							</ul>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6 col-xl-4">
					<div class="row row--grid">
						<!-- title -->
						<div class="col-12">
							<div class="main__title">
								<h2><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19.12,2.21A1,1,0,0,0,18.26,2l-8,2A1,1,0,0,0,9.5,5V15.35A3.45,3.45,0,0,0,8,15a3.5,3.5,0,1,0,3.5,3.5V10.78L18.74,9l.07,0L19,8.85l.15-.1a.93.93,0,0,0,.13-.15.78.78,0,0,0,.1-.15.55.55,0,0,0,.06-.18.58.58,0,0,0,0-.19.24.24,0,0,0,0-.08V3A1,1,0,0,0,19.12,2.21ZM8,20a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,8,20ZM17.5,7.22l-6,1.5V5.78l6-1.5Z"/></svg><a href="#">New Singles</a></h2>
							</div>
						</div>
						<!-- end title -->

						<div class="col-12">
							<ul class="main__list">
							
							@foreach($posts as $post)
								@if($post->status == 1)
								<li class="single-item">
									
									<a data-link data-title="{{ $post->title}}"

									@foreach($artists as $artist)
										@if($artist->id == $post->artist_id)
											data-artist="{{ $artist->title }}"                                      
											data-img="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}"                                   
										@endif
									@endforeach	
									
									href="{{ $post->audio_path_url == null ? asset('audio').'/'.$post->audio_path : $post->audio_path_url }}" class="update{{$post->id}} single-item__cover">

									@foreach($artists as $artist)
										@if($artist->id == $post->artist_id)
                                        <img src="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}" alt="sdsd">
                                    	@endif
									@endforeach	

										<!-- <img src="{{ asset('assets/img/covers/cover1.jpg')}}" alt=""> -->

										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"/></svg>
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M16,2a3,3,0,0,0-3,3V19a3,3,0,0,0,6,0V5A3,3,0,0,0,16,2Zm1,17a1,1,0,0,1-2,0V5a1,1,0,0,1,2,0ZM8,2A3,3,0,0,0,5,5V19a3,3,0,0,0,6,0V5A3,3,0,0,0,8,2ZM9,19a1,1,0,0,1-2,0V5A1,1,0,0,1,9,5Z"/></svg>
									</a>
									<!-- how many times are play  -->
									<script type='text/javascript'>
									var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
										$(document).on("click", ".update{{$post->id}}" , function() {
											var edit_id = {{ $post->id}};

											var name = {{ $post->download_count}} + 1;
												$.ajax({
												url: '/dashboard/post/{{ $post->id}}',
												type: 'patch',
												data: {_token: CSRF_TOKEN,id: edit_id,download_count: name},
												success: function(response){
													alert(response);
												}
												});											 
											}); 
										</script>
									<!-- how many times are play  -->
									<div class="single-item__title">
										<h4><a href="{{ url('post/listen/'.$post->id) }}">{{ substr($post->title, 0, 20)}}</a></h4>
										<span>
										@foreach($artists as $artist)
											@if($artist->id == $post->artist_id)
											<a href="{{ url('artist/'.$artist->id) }}">
												{{ $artist->title }}
											</a>
											@endif
										@endforeach 
										</span>
									</div>
									<span class="single-item__time">{{ $post->duration}}</span>
								</li>								
								@endif
							@endforeach 								
							</ul>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6 col-xl-4">
					<div class="row row--grid">
						
						<div class="col-12">
							<div class="main__title">
								<h2><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,15a4,4,0,0,0,4-4V5A4,4,0,0,0,8,5v6A4,4,0,0,0,12,15ZM10,5a2,2,0,0,1,4,0v6a2,2,0,0,1-4,0Zm10,6a1,1,0,0,0-2,0A6,6,0,0,1,6,11a1,1,0,0,0-2,0,8,8,0,0,0,7,7.93V21H9a1,1,0,0,0,0,2h6a1,1,0,0,0,0-2H13V18.93A8,8,0,0,0,20,11Z"/></svg><a href="#">Podcasts</a></h2>
							</div>
						</div>
						<div class="col-12">
							<ul class="main__list">
							@foreach($posts as $post)
								@if($post->status == 1)
								<li class="single-item">									
									<a data-link data-title="{{ $post->title}}"
									@foreach($artists as $artist)
										@if($artist->id == $post->artist_id)
											data-artist="{{ $artist->title }}"                                      
											data-img="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}"                                   
										@endif
									@endforeach	
									
									href="{{ $post->audio_path_url == null ? asset('audio').'/'.$post->audio_path : $post->audio_path_url }}" class="update{{$post->id}} single-item__cover">

									@foreach($artists as $artist)
										@if($artist->id == $post->artist_id)
                                        <img src="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}" alt="sdsd">
                                    	@endif
									@endforeach	
										<!-- <img src="{{ asset('assets/img/covers/cover1.jpg')}}" alt=""> -->
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"/></svg>
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M16,2a3,3,0,0,0-3,3V19a3,3,0,0,0,6,0V5A3,3,0,0,0,16,2Zm1,17a1,1,0,0,1-2,0V5a1,1,0,0,1,2,0ZM8,2A3,3,0,0,0,5,5V19a3,3,0,0,0,6,0V5A3,3,0,0,0,8,2ZM9,19a1,1,0,0,1-2,0V5A1,1,0,0,1,9,5Z"/></svg>
									</a>
									<!-- how many times are play  -->
									<script type='text/javascript'>
									var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
										$(document).on("click", ".update{{$post->id}}" , function() {
											var edit_id = {{ $post->id}};

											var name = {{ $post->download_count}} + 1;
												$.ajax({
												url: '/dashboard/post/{{ $post->id}}',
												type: 'patch',
												data: {_token: CSRF_TOKEN,id: edit_id,download_count: name},
												success: function(response){
													alert(response);
												}
												});											 
											}); 
										</script>
									<!-- how many times are play  -->
									<div class="single-item__title">
										<h4><a href="{{ url('post/listen/'.$post->id) }}">{{ substr($post->title, 0, 20)}}</a></h4>
										<span>
										@foreach($artists as $artist)
											@if($artist->id == $post->artist_id)
											<a href="{{ url('artist/'.$artist->id) }}">
												{{ $artist->title }}
											</a>
											@endif
										@endforeach 
										</span>
									</div>
									<span class="single-item__time single-item__time--live">LIVE</span>
								</li>								 
								@endif
							@endforeach 								
								
							</ul>
						</div>
					</div>
				</div>
			</section>

			<!-- podcasts -->
			<!-- <section class="row row--grid">
				
				<div class="col-12">
					<div class="main__title">
						<h2>Podcasts</h2>

						<a href="podcasts.html" class="main__link">See all <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></a>
					</div>
				</div>
			

				<div class="col-12">
					<div class="main__carousel-wrap">
						<div class="main__carousel main__carousel--podcasts owl-carousel" id="podcasts">
							<div class="live">
								<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="live__cover open-video">
									<img src="{{ asset('assets/img/live/1.jpg')}}" alt="">
									<span class="live__status">live</span>
									<span class="live__value">6.5K viewers</span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"></path></svg>
								</a>
								<h3 class="live__title"><a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="open-video">Beautiful Stories From Anonymous People</a></h3>
							</div>

							<div class="live">
								<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="live__cover open-video">
									<img src="{{ asset('assets/img/live/2.jpg')}}" alt="">
									<span class="live__status">live</span>
									<span class="live__value">1.7K viewers</span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"></path></svg>
								</a>
								<h3 class="live__title"><a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="open-video">Song Exploder</a></h3>
							</div>

							<div class="live">
								<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="live__cover open-video">
									<img src="{{ asset('assets/img/live/3.jpg')}}" alt="">
									<span class="live__status">live</span>
									<span class="live__value">924 viewers</span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"></path></svg>
								</a>
								<h3 class="live__title"><a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="open-video">Broken Record</a></h3>
							</div>

							<div class="live">
								<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="live__cover open-video">
									<img src="{{ asset('assets/img/live/4.jpg')}}" alt="">
									<span class="live__status">live</span>
									<span class="live__value">638 viewers</span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"></path></svg>
								</a>
								<h3 class="live__title"><a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="open-video">Desert Island Discs</a></h3>
							</div>

							<div class="live">
								<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="live__cover open-video">
									<img src="{{ asset('assets/img/live/5.jpg')}}" alt="">
									<span class="live__value">588 views</span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"></path></svg>
								</a>
								<h3 class="live__title"><a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="open-video">Riffs On Riffs</a></h3>
							</div>

							<div class="live">
								<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="live__cover open-video">
									<img src="{{ asset('assets/img/live/6.jpg')}}" alt="">
									<span class="live__value">588 views</span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"></path></svg>
								</a>
								<h3 class="live__title"><a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="open-video">Popcast</a></h3>
							</div>

							<div class="live">
								<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="live__cover open-video">
									<img src="{{ asset('assets/img/live/7.jpg')}}" alt="">
									<span class="live__value">6.5K views</span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"></path></svg>
								</a>
								<h3 class="live__title"><a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="open-video">Rolling Stone Music Now</a></h3>
							</div>

							<div class="live">
								<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="live__cover open-video">
									<img src="{{ asset('assets/img/live/8.jpg')}}" alt="">
									<span class="live__value">1.7K views</span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"></path></svg>
								</a>
								<h3 class="live__title"><a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="open-video">Song vs. Song</a></h3>
							</div>

							<div class="live">
								<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="live__cover open-video">
									<img src="{{ asset('assets/img/live/9.jpg')}}" alt="">
									<span class="live__value">924 views</span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"></path></svg>
								</a>
								<h3 class="live__title"><a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="open-video">Disgraceland</a></h3>
							</div>
						</div>

						<button class="main__nav main__nav--prev" data-nav="#podcasts" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,11H9.41l3.3-3.29a1,1,0,1,0-1.42-1.42l-5,5a1,1,0,0,0-.21.33,1,1,0,0,0,0,.76,1,1,0,0,0,.21.33l5,5a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L9.41,13H17a1,1,0,0,0,0-2Z"/></svg></button>
						<button class="main__nav main__nav--next" data-nav="#podcasts" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></button>
					</div>
				</div>
			</section> -->
			<!-- end podcasts -->

			<!-- store -->
			<!-- <section class="row row--grid">
			 
				<div class="col-12">
					<div class="main__title">
						<h2>Products</h2>

						<a href="store.html" class="main__link">See all <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></a>
					</div>
				</div>
		 

				<div class="col-12">
					<div class="main__carousel-wrap">
						<div class="main__carousel main__carousel--store owl-carousel" id="store">
							<div class="product">
								<div class="product__img">
									<img src="{{ asset('assets/img/store/item1.jpg')}}" alt="">

									<button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
								</div>
								<h3 class="product__title"><a href="product.html">Vinyl Player</a></h3>
								<span class="product__price">$1 099</span>
								<span class="product__new">New</span>
							</div>

							<div class="product">
								<div class="product__img">
									<img src="{{ asset('assets/img/store/item2.jpg')}}" alt="">

									<button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
								</div>
								<h3 class="product__title"><a href="product.html">Microphone R4</a></h3>
								<span class="product__price">$799</span>
							</div>

							<div class="product">
								<div class="product__img">
									<img src="{{ asset('assets/img/store/item3.jpg')}}" alt="">

									<button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
								</div>
								<h3 class="product__title"><a href="product.html">Music Blank</a></h3>
								<span class="product__price">$3.99</span>
								<span class="product__new">New</span>
							</div>

							<div class="product">
								<div class="product__img">
									<img src="{{ asset('assets/img/store/item4.jpg')}}" alt="">

									<button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
								</div>
								<h3 class="product__title"><a href="product.html">Headphones ZR-991</a></h3>
								<span class="product__price">$199</span>
							</div>

							<div class="product">
								<div class="product__img">
									<img src="{{ asset('assets/img/store/item5.jpg')}}" alt="">

									<button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
								</div>
								<h3 class="product__title"><a href="product.html">Piano</a></h3>
								<span class="product__price">$11 899</span>
							</div>

							<div class="product">
								<div class="product__img">
									<img src="{{ asset('assets/img/store/item6.jpg')}}" alt="">

									<button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
								</div>
								<h3 class="product__title"><a href="product.html">Vinyl Player</a></h3>
								<span class="product__price">$2 499</span>
							</div>

							<div class="product">
								<div class="product__img">
									<img src="{{ asset('assets/img/store/item7.jpg')}}" alt="">

									<button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
								</div>
								<h3 class="product__title"><a href="product.html">Guitar</a></h3>
								<span class="product__price">$299</span>
							</div>

							<div class="product">
								<div class="product__img">
									<img src="{{ asset('assets/img/store/item8.jpg')}}" alt="">

									<button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
								</div>
								<h3 class="product__title"><a href="product.html">Microphone R4s</a></h3>
								<span class="product__price">$199</span>
							</div>
						</div>

						<button class="main__nav main__nav--prev" data-nav="#store" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,11H9.41l3.3-3.29a1,1,0,1,0-1.42-1.42l-5,5a1,1,0,0,0-.21.33,1,1,0,0,0,0,.76,1,1,0,0,0,.21.33l5,5a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L9.41,13H17a1,1,0,0,0,0-2Z"/></svg></button>
						<button class="main__nav main__nav--next" data-nav="#store" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></button>
					</div>
				</div>
			</section> -->
			<!-- end store -->

		</div>
	</main>
@endsection
