@extends('master')
@section('content')
	<!-- main content -->
	<main class="main">
		<div class="container-fluid">
			<div class="row row--grid">
				<!-- breadcrumb -->
				<div class="col-12">
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">{{$page->title}}</li>
					</ul>
				</div>
				<!-- end breadcrumb -->

				<!-- title -->
				<div class="col-12">
					<div class="main__title">
						<h1>{{$page->title}}</h1>

						<p>{!!$page->content!!}</p>
					</div>
				</div>
				<!-- end title -->


				
			</div>
		</div>
	</main>
	<!-- end main content -->
 
@endsection