@extends('master')
@section('content')

	<!-- main content -->
	<main class="main">
		<div class="container-fluid">
			<!-- artists -->
			<div class="row row--grid">
				<!-- breadcrumb -->
				<div class="col-12">
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">Page</li>
					</ul>
				</div>
				<!-- end breadcrumb -->
                <!-- title -->
				<div class="col-12">
                    <div class="row">
                    @if ($errors->any())
                    <div class="alert alert-danger sign__group">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger">{{ $error }}</li>                                            
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-{{ session('type') }} sign__group">
                            {{session('message')}}
                        </div>
                    @endif                    
                    </div>
				</div>

                <div class="row row--grid">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <form action="{{ url('/dashboard/page') }}" method="post" enctype="multipart/form-data" class="sign__form sign__form--contacts">
                        {{ csrf_field() }}
                            <!-- <div class="row">  -->

                                <div class="col-12 col-md-12">
                                    <div class="sign__group">
                                        <input type="hidden" name="user_id" class="sign__input" value="0" placeholder="user id">

                                        <input type="text" name="title" class="sign__input" placeholder="Title">                                        
                                    </div>
                                </div>                                

                                <div class="col-12">
                                    <div class="sign__group">
                                        <textarea name="content" class="sign__textarea" placeholder="Content"></textarea>
                                    </div>
                                </div> 
                                <div class="col-12">
                                    <div class="sign__group"> 
                                        <select name="status" class="sign__input" placeholder="status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-xl-12">
                                    <button type="submit" class="sign__btn">Save</button>
                                </div>

                            <!-- </div> -->

                        </form>	
                    </div>                  
                </div>                
			</div>
		</div>
	</main>
	<!-- end main content -->

    <script>
        tinymce.init({
            selector: 'textarea',
            height: 550,
            directionality: '',
            language: '',
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount textpattern noneditable help charmap emoticons', // imagetools, quickbars
            imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
            toolbar_sticky: false,
            document_base_url: '{{url('/')}}',
            relative_urls: true,
            convert_urls: false,
            toolbar_mode: 'sliding',
            file_picker_callback(callback, value, meta) {
                let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth
                let y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight

                tinymce.activeEditor.windowManager.openUrl({
                    url: '{{url('/dashboard/mediamanager')}}',
                    title: '{{ __("Media Library") }}',
                    width: x * 0.8,
                    height: y * 0.8,
                    onMessage: (api, message) => {
                        callback(message.content, {text: message.text})
                    }
                })
            }
        });
    </script>
@endsection