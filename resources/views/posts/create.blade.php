@extends('master')
@section('content')

	<!-- main content -->
	<main class="main">
		<div class="container-fluid">
			<!-- artists -->
			<div class="row row--grid">
				<!-- breadcrumb -->
				<div class="col-12">
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">Audio</li>
					</ul>
				</div>
				<!-- end breadcrumb -->
                <!-- title -->
				<div class="col-12">
                    <div class="row">
                    @if ($errors->any())
                    <div class="alert alert-danger sign__group">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger">{{ $error }}</li>                                            
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-{{ session('type') }} sign__group">
                            {{session('message')}}
                        </div>
                    @endif                    
                    </div>
				</div>

                <div class="row row--grid">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <form action="{{ url('/dashboard/post') }}" method="post" enctype="multipart/form-data" class="sign__form sign__form--contacts">
                        {{ csrf_field() }}
                            <div class="row">
                                
                                <div class="col-12 col-md-12">
                                    <div class="sign__group">                                          
                                        <select name="artist_id" class="sign__input" placeholder="Artist Name">
                                            <option value="0" selected>Artist Name</option>
                                            @foreach($artists as $artist)
                                            <option value="{{ $artist->id }}">{{ $artist->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="sign__group">                                         
                                        <select name="release_id" class="sign__input" placeholder="Release Name">
                                            <option value="0" selected>Release Name</option>
                                            @foreach($releases as $release)
                                            <option value="{{ $release->id }}">{{ $release->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="sign__group">
                                        <input type="hidden" name="user_id" class="sign__input" value="0" placeholder="user id">

                                        <input type="text" name="title" class="sign__input" placeholder="Title">                                        
                                    </div>
                                </div>                                

                                <div class="col-12">
                                    <div class="sign__group">
                                        <textarea name="content" class="sign__textarea" placeholder="Content"></textarea>
                                    </div>
                                </div>                            

                                <div class="col-12">
                                    <div class="sign__group">                                    
                                        <input type="text" name="duration" class="sign__input" placeholder="Duration">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="sign__group">
                                        <input type="file" name="audio_path" class="sign__input img-p">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="sign__group">
                                        <input type="text" name="audio_path_url" class="sign__input img-p" placeholder="Audio Externel url">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="sign__group"> 
                                        <select name="download_trun" class="sign__input" placeholder="Download Status">
                                            <option value="1">Download On</option>
                                            <option value="0">Download Off</option>
                                        </select>
                                    </div>
                                </div> 

                                <div class="col-12">
                                    <div class="sign__group"> 
                                        <select name="status" class="sign__input" placeholder="status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-xl-12">
                                    <button type="submit" class="sign__btn">Save</button>
                                </div>
                            </div>
                        </form>	
                    </div>                  
                </div>                
			</div>
		</div>
	</main>
	<!-- end main content -->
@endsection