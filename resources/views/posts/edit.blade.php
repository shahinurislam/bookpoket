@extends('master')
@section('content')

	<!-- main content -->
	<main class="main">
		<div class="container-fluid">
			<!-- artists -->
			<div class="row row--grid">
				<!-- breadcrumb -->
				<div class="col-12">
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">Audio</li>
					</ul>
				</div>
				<!-- end breadcrumb -->

                <div class="row row--grid">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <form action="{{ url('/dashboard/post') }}/{{ $post->id }}" method="post" enctype="multipart/form-data" class="sign__form sign__form--contacts">
                        {{ csrf_field() }}
                        @method('PATCH')
                            <div class="row">
                            <div class="col-12 col-md-12">
                                    <div class="sign__group">                                          
                                        <select name="artist_id" class="sign__input" placeholder="Artist Name">
                                            <option value="0" selected>Artist Name</option>
                                            @foreach($artists as $artist)
                                            <option value="{{ $artist->id }}" {{ $artist->id == $post->artist_id ? 'selected' : '' }}>{{ $artist->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="sign__group">                                         
                                        <select name="release_id" class="sign__input" placeholder="Release Name">
                                            <option value="0" selected>Release Name</option>
                                            @foreach($releases as $release)
                                            <option value="{{ $release->id }}" {{ $release->id == $post->release_id ? 'selected' : '' }} >{{ $release->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="sign__group">
                                        <input type="hidden" name="user_id" class="sign__input" value="0" placeholder="user id">

                                        <input type="text" name="title" class="sign__input" placeholder="Name" value="{{ $post->title }}"/>
                                    </div>
                                </div>                                

                                <div class="col-12">
                                    <div class="sign__group">
                                        <textarea name="content" class="sign__textarea" placeholder="Content" value='{{ $post->content }}'>{{ $post->content }}</textarea>
                                    </div>
                                </div>
                                
                                <div class="col-12">
                                    <div class="sign__group">                                    
                                        <input type="text" name="duration" class="sign__input" placeholder="Duration" value='{{ $post->duration }}'>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="sign__group">
                                        <input type="hidden" name="audio_path_re" class="sign__input img-p"  value="{{ $post->audio_path }}" />
                                        <input type="file" name="audio_path" class="sign__input img-p">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="sign__group">
                                        <input type="text" name="audio_path_url" class="sign__input img-p"  value="{{ $post->audio_path_url }}" placeholder="Audio Externel url"/>                                        
                                    </div>
                                </div>                                
                                
                                <div class="col-12">
                                    <div class="sign__group"> 
                                        <select name="download_trun" class="sign__input" placeholder="Download Status">
                                            <option value="1" {{ $post->download_trun == 1 ? 'selected' : '' }} >Download On</option>
                                            <option value="0" {{ $post->download_trun == 0 ? 'selected' : '' }} >Download Off</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="sign__group"> 
                                        <select name="status" class="sign__input" placeholder="status">
                                            <option value="1" {{ $post->status == 1 ? 'selected' : '' }} >Active</option>
                                            <option value="0" {{ $post->status == 0 ? 'selected' : '' }} >Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-xl-12">
                                    <button type="submit" class="sign__btn">Save</button>
                                </div>
                            </div>
                        </form>	
                    </div>                  
                </div>                
			</div>
		</div>
	</main>
	<!-- end main content -->
@endsection