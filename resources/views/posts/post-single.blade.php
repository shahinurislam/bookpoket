@extends('master')
@section('content')
<!-- main content -->
<main class="main">
    <div class="container-fluid">
        <div class="row row--grid">
            <!-- breadcrumb -->
            <div class="col-12">
                <ul class="breadcrumb">
                    <li class="breadcrumb__item"><a href="{{url('/')}}">Home</a></li>
                    <li class="breadcrumb__item"><a href="store.html">Store</a></li>
                    <li class="breadcrumb__item breadcrumb__item--active">Play</li>
                </ul>
            </div>
            <!-- end breadcrumb -->

            <!-- title -->
            <div class="col-12">
                <div class="main__title main__title--page">
                    <h1>{{ $post->title}} - 
                        @foreach($artists as $artist)
							@if($artist->id == $post->artist_id)
								{{ $artist->title }}
							@endif
						@endforeach	
                    </h1>
                </div>
            </div>
            <!-- end title -->

            <div class="col-12">
                <div class="store-item">
                    <div class="store-item__content">
                        <div class="store-item__carousel owl-carousel">
                            <div class="store-item__cover main__list main__list--playlist main__list--dashbox single-plyer">
							 
                                
                                <a data-playlist data-title="{{ $post->title}}" 

                                    @foreach($artists as $artist)
										@if($artist->id == $post->artist_id)
											data-artist="{{ $artist->title }}"                                            
                                            data-img="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}"                                            
										@endif
									@endforeach	

                                href="{{ $post->audio_path_url == null ? asset('audio').'/'.$post->audio_path : $post->audio_path_url }}" class="updateAudioView single-item__cover">

                                @foreach($artists as $artist)
									@if($artist->id == $post->artist_id)
                                        <img src="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}" alt="sdsd">
                                    @endif
								@endforeach	
                                <!-- <img src="{{ asset('assets/img/covers/cover.svg')}}" alt=""> -->
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"/></svg>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M16,2a3,3,0,0,0-3,3V19a3,3,0,0,0,6,0V5A3,3,0,0,0,16,2Zm1,17a1,1,0,0,1-2,0V5a1,1,0,0,1,2,0ZM8,2A3,3,0,0,0,5,5V19a3,3,0,0,0,6,0V5A3,3,0,0,0,8,2ZM9,19a1,1,0,0,1-2,0V5A1,1,0,0,1,9,5Z"/></svg>
                                </a>
                                <!-- how many times are play  -->
                                <script type='text/javascript'>
                                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                                    $(document).on("click", ".updateAudioView" , function() {
                                        var edit_id = {{ $post->id}};
                                        var name = {{ $post->download_count}} + 1;
                                            $.ajax({
                                            url: '/dashboardajax/post/{{ $post->id}}',
                                            type: 'patch',
                                            data: {_token: CSRF_TOKEN,id: edit_id,download_count: name},
                                            // success: function(response){
                                            //     alert(response);
                                            // }
                                            });											 
                                        }); 
                                    </script>
                                <!-- how many times are play  -->
 
                            </div>
                        </div>
                    </div>

                    <div class="store-item__description">
                        <!-- article content -->
                        <div class="article__content">
                            <h4>About {{ $post->title}}</h4>
                            <span class="article__price">{{ $post->duration}} 
								<svg width="20px" height="20px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><defs><style>.cls-1{fill:#71c6c4;}.cls-2{fill:#dde5e8;}.cls-3{fill:#afc3c9;}</style></defs><title>wall-clock-flat</title><circle class="cls-1" cx="256" cy="256" r="256"/><circle class="cls-2" cx="256" cy="256" r="204.8"/><path class="cls-3" d="M354.65,354.65a12.8,12.8,0,0,1-18.1,0l-64-64a38.4,38.4,0,1,1-29.35-70.86v-79a12.8,12.8,0,1,1,25.6,0v79a38.42,38.42,0,0,1,21.86,52.76l64,64A12.8,12.8,0,0,1,354.65,354.65Z"/></svg>
							</span>
							<span class="article__price">{{ $post->download_count}} 
								<svg width="20px" height="20px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 122.88 68.18"><defs><style>.cls-1{fill-rule:evenodd;}</style></defs><title>view</title><path class="cls-1" d="M61.44,13.81a20.31,20.31,0,1,1-14.34,6,20.24,20.24,0,0,1,14.34-6ZM1.05,31.31A106.72,106.72,0,0,1,11.37,20.43C25.74,7.35,42.08.36,59,0s34.09,5.92,50.35,19.32a121.91,121.91,0,0,1,12.54,12,4,4,0,0,1,.25,5,79.88,79.88,0,0,1-15.38,16.41A69.53,69.53,0,0,1,63.43,68.18,76,76,0,0,1,19.17,53.82,89.35,89.35,0,0,1,.86,36.44a3.94,3.94,0,0,1,.19-5.13Zm15.63-5A99.4,99.4,0,0,0,9.09,34,80.86,80.86,0,0,0,23.71,47.37,68.26,68.26,0,0,0,63.4,60.3a61.69,61.69,0,0,0,38.41-13.72,70.84,70.84,0,0,0,12-12.3,110.45,110.45,0,0,0-9.5-8.86C89.56,13.26,74.08,7.58,59.11,7.89S29.63,14.48,16.68,26.27Zm39.69-7.79a7.87,7.87,0,1,1-7.87,7.87,7.86,7.86,0,0,1,7.87-7.87Z"/></svg>
							</span>


                            <p>{{ $post->content}}</p>
                            <a href="#" class="article__buy">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                </svg>{{ $post->heart_count}}
                            </a> 
                            <!-- how many times are love  -->
                            <script type='text/javascript'>
                                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                                    $(document).on("click", ".article__buy" , function() {
                                        var edit_id = {{ $post->id}};

                                        var name = {{ $post->heart_count}} + 1;
                                            $.ajax({
                                            url: '/dashboardajax/post/{{ $post->id}}',
                                            type: 'patch',
                                            data: {_token: CSRF_TOKEN,id: edit_id,heart_count: name},
                                            success: function(response){
                                                alert("You love this audio");
                                            }
                                            });											 
                                        }); 
                            </script>
                                <!-- how many times are love  -->
                        </div>
                        <!-- end article content -->

                        <!-- share -->
                        <div class="share">
                            <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}&display=popup" class="share__link share__link--fb"><svg width="9" height="17" viewBox="0 0 9 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.56341 16.8197V8.65888H7.81615L8.11468 5.84663H5.56341L5.56724 4.43907C5.56724 3.70559 5.63693 3.31257 6.69042 3.31257H8.09873V0.5H5.84568C3.1394 0.5 2.18686 1.86425 2.18686 4.15848V5.84695H0.499939V8.6592H2.18686V16.8197H5.56341Z"/></svg> share</a>
                            <a href="https://twitter.com/intent/tweet?url={{url()->current()}}" class="share__link share__link--tw"><svg width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.55075 3.19219L7.58223 3.71122L7.05762 3.64767C5.14804 3.40404 3.47978 2.57782 2.06334 1.1902L1.37085 0.501686L1.19248 1.01013C0.814766 2.14353 1.05609 3.34048 1.843 4.14552C2.26269 4.5904 2.16826 4.65396 1.4443 4.38914C1.19248 4.3044 0.972149 4.24085 0.951164 4.27263C0.877719 4.34677 1.12953 5.31069 1.32888 5.69202C1.60168 6.22165 2.15777 6.74068 2.76631 7.04787L3.28043 7.2915L2.67188 7.30209C2.08432 7.30209 2.06334 7.31268 2.12629 7.53512C2.33613 8.22364 3.16502 8.95452 4.08833 9.2723L4.73884 9.49474L4.17227 9.8337C3.33289 10.321 2.34663 10.5964 1.36036 10.6175C0.888211 10.6281 0.5 10.6705 0.5 10.7023C0.5 10.8082 1.78005 11.4014 2.52499 11.6344C4.75983 12.3229 7.41435 12.0264 9.40787 10.8506C10.8243 10.0138 12.2408 8.35075 12.9018 6.74068C13.2585 5.88269 13.6152 4.315 13.6152 3.56293C13.6152 3.07567 13.6467 3.01212 14.2343 2.42953C14.5805 2.09056 14.9058 1.71983 14.9687 1.6139C15.0737 1.41264 15.0632 1.41264 14.5281 1.59272C13.6362 1.91049 13.5103 1.86812 13.951 1.39146C14.2762 1.0525 14.6645 0.438131 14.6645 0.258058C14.6645 0.22628 14.5071 0.279243 14.3287 0.374576C14.1398 0.480501 13.7202 0.639389 13.4054 0.734722L12.8388 0.914795L12.3247 0.565241C12.0414 0.374576 11.6427 0.162725 11.4329 0.0991699C10.8978 -0.0491255 10.0794 -0.0279404 9.59673 0.14154C8.2852 0.618204 7.45632 1.84694 7.55075 3.19219Z"/></svg> tweet</a>
                            <a href="http://vk.com/share.php?url={{url()->current()}}&title={{$post->title}}" class="share__link share__link--vk"><svg width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.78479 8.92255C8.78479 8.92255 9.07355 8.89106 9.22145 8.73512C9.35684 8.59224 9.35214 8.32262 9.35214 8.32262C9.35214 8.32262 9.33414 7.06361 9.92967 6.87771C10.5166 6.69489 11.2702 8.09524 12.07 8.63372C12.6741 9.04085 13.1327 8.95174 13.1327 8.95174L15.2699 8.92255C15.2699 8.92255 16.3874 8.85495 15.8576 7.99231C15.8137 7.92164 15.5485 7.35397 14.269 6.1879C12.9284 4.9673 13.1084 5.16472 14.7221 3.05305C15.705 1.76715 16.0978 0.982093 15.975 0.646407C15.8584 0.325317 15.1353 0.410582 15.1353 0.410582L12.7297 0.425177C12.7297 0.425177 12.5513 0.401365 12.419 0.478949C12.2899 0.554996 12.2061 0.732441 12.2061 0.732441C12.2061 0.732441 11.8258 1.72721 11.3179 2.57372C10.2466 4.35892 9.81855 4.4534 9.64326 4.34279C9.23554 4.08392 9.33727 3.30424 9.33727 2.75039C9.33727 1.01973 9.60491 0.298431 8.81687 0.111769C8.5555 0.0495478 8.36299 0.00883541 7.6939 0.00192196C6.83543 -0.00652779 6.10921 0.00499461 5.69758 0.202411C5.42369 0.333767 5.2124 0.627203 5.34152 0.644103C5.50038 0.664843 5.86036 0.739354 6.0513 0.994383C6.29781 1.32392 6.2892 2.06289 6.2892 2.06289C6.2892 2.06289 6.43084 4.10005 5.95818 4.35277C5.6342 4.52638 5.1897 4.17226 4.2342 2.55221C3.7451 1.7226 3.37573 0.805416 3.37573 0.805416C3.37573 0.805416 3.30451 0.634117 3.17696 0.541938C3.02279 0.430555 2.80759 0.395987 2.80759 0.395987L0.521729 0.410582C0.521729 0.410582 0.178185 0.4198 0.0521924 0.566519C-0.0597138 0.696338 0.0435842 0.965961 0.0435842 0.965961C0.0435842 0.965961 1.8333 5.07638 3.86013 7.1481C5.71871 9.04699 7.8285 8.92255 7.8285 8.92255H8.78479Z"/></svg> share</a>
                        </div>
                        <!-- end share -->
                    </div>
                </div>
            </div>
        </div> 

        <!-- comments -->
       <section class="row row--grid">
            <div class="col-12">
                <!-- comments -->
                <div class="comments">
                    <div class="comments__title">
                        <h4>Comments</h4>
                        <span>
                        <!-- comment count -->
                        @php $commentCount = 0; @endphp
                        @foreach($comments as $comment)
                            @if($comment->post_id == $post->id)
                                @php $commentCount+= 1; @endphp
                            @endif
                        @endforeach
                        {{ $commentCount }}  
                        <!-- comment count end--> 
                        </span>
                    </div>
                    @php
                    $usrCurrentId = optional(auth()->user())->id;
                    @endphp
                    <ul class="comments__list">
                        @foreach($comments as $comment)
                            @if($comment->post_id == $post->id && $comment->reply_id == null)                            
                                <li class="comments__item" id="{{$comment->id}}">
                                    <div class="comments__autor">
                                    @foreach($users as $user)
                                        @if($user->id == $comment->user_id )
                                            
                                        <img class="comments__avatar" src="{{ asset('assets/img/avatar.svg')}}" alt="">
                                        <span class="comments__name">{{$user->name}}</span>
                                            
                                        @endif
                                    @endforeach
                                        <span class="comments__time">{{$comment->created_at}}</span>
                                    </div>
                                    <p class="comments__text @if(session()->has('message'.$comment->id)) alert alert-{{session('message'.$comment->id)}} @endif">{{$comment->comment}}</p>
                                    <div class="comments__actions">
                                        <!-- <div class="comments__rate">
                                            <button type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg> 12</button> --> 
                                        <!-- condition for comment woner to permission delte -->    
                                                                     
                                        @if($comment->user_id == $usrCurrentId )    
                                        <div class="comments__rate">
                                            <form action="{{ url('comments/') }}/{{ $comment->id }}" method="POST">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            <input type="hidden" name="post_id" value="{{ $comment->post_id }}"/>                                           
                                            <button type="submit"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0 0 30 30" style=" fill:#000000;">    <path d="M 14.984375 2.4863281 A 1.0001 1.0001 0 0 0 14 3.5 L 14 4 L 8.5 4 A 1.0001 1.0001 0 0 0 7.4863281 5 L 6 5 A 1.0001 1.0001 0 1 0 6 7 L 24 7 A 1.0001 1.0001 0 1 0 24 5 L 22.513672 5 A 1.0001 1.0001 0 0 0 21.5 4 L 16 4 L 16 3.5 A 1.0001 1.0001 0 0 0 14.984375 2.4863281 z M 6 9 L 7.7929688 24.234375 C 7.9109687 25.241375 8.7633438 26 9.7773438 26 L 20.222656 26 C 21.236656 26 22.088031 25.241375 22.207031 24.234375 L 24 9 L 6 9 z"></path></svg></button></form>
                                        </div>
                                        @endif
                                        <script>
                                            function changeValue{{$comment->id}}(o){
                                                document.getElementById('type{{$comment->id}}').value=o;
                                                document.getElementById("replay{{$comment->id}}").style.display = "block";
                                                document.getElementById('typeQuote{{$comment->id}}').value="<b>{{$comment->comment}}</b>";
                                            }                                            
                                        </script>
                                        <button onclick="changeValue{{$comment->id}}('{{$comment->id}}')"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M21.707,11.293l-8-8A.99991.99991,0,0,0,12,4V7.54492A11.01525,11.01525,0,0,0,2,18.5V20a1,1,0,0,0,1.78418.62061,11.45625,11.45625,0,0,1,7.88672-4.04932c.0498-.00635.1748-.01611.3291-.02588V20a.99991.99991,0,0,0,1.707.707l8-8A.99962.99962,0,0,0,21.707,11.293ZM14,17.58594V15.5a.99974.99974,0,0,0-1-1c-.25488,0-1.2959.04932-1.56152.085A14.00507,14.00507,0,0,0,4.05176,17.5332,9.01266,9.01266,0,0,1,13,9.5a.99974.99974,0,0,0,1-1V6.41406L19.58594,12Z"/></svg><span >Reply</span></button>
                                        <!-- <button><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,14a1,1,0,0,0-1.22.72A7,7,0,0,1,11,20H5.41l.64-.63a1,1,0,0,0,0-1.41A7,7,0,0,1,9.25,6.22a1,1,0,0,0-.5-1.94A9,9,0,0,0,4,18.62L2.29,20.29a1,1,0,0,0-.21,1.09A1,1,0,0,0,3,22h8a9,9,0,0,0,8.72-6.75A1,1,0,0,0,19,14ZM21,2a1,1,0,0,0-1,1h0a5,5,0,1,0,.3,7.75A1,1,0,1,0,19,9.25,3,3,0,1,1,17,4a3,3,0,0,1,2.23,1H18a1,1,0,0,0,0,2h3a1,1,0,0,0,1-1V3A1,1,0,0,0,21,2Z"/></svg><span>Quote</span></button> -->
                                    </div>
                                    @auth()
                                    <form action="{{ url('/comments') }}" method="post" enctype="multipart/form-data" class="comments__form displayNone" id="replay{{$comment->id}}">
                                        {{ csrf_field() }}
                                        <div class="sign__group">
                                            <input type="hidden" name="post_id" value="{{ $post->id}}" class="sign__input" placeholder="{{ $post->id}}"> 
                                            <input type="hidden" name="user_id" value="{{ optional(auth()->user())->id}}" class="sign__input" placeholder="{{ optional(auth()->user())->id}}"> 
                                            <input type="hidden" id="type{{$comment->id}}" name="reply_id" class="sign__input" placeholder="reply_id">                                             
                                            <input type="hidden" id="typeQuote{{$comment->id}}" name="changeQuote" class="sign__input" placeholder="Quite Comment">  <!-- replay comment with quite -->
                                            <textarea id="text" name="comment" class="sign__textarea" placeholder="Add comment"></textarea>
                                        </div>
                                        <button type="submit" class="sign__btn">Send</button>
                                    </form>                                    
                                    @endauth
                                    @guest()
                                    <a class="header__action-btn" href="{{ url('/signin')}}">
                                        <span>Sign in comment reply</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.5,15.1a1,1,0,0,0-1.34.45A8,8,0,1,1,12,4a7.93,7.93,0,0,1,7.16,4.45,1,1,0,0,0,1.8-.9,10,10,0,1,0,0,8.9A1,1,0,0,0,20.5,15.1ZM21,11H11.41l2.3-2.29a1,1,0,1,0-1.42-1.42l-4,4a1,1,0,0,0-.21.33,1,1,0,0,0,0,.76,1,1,0,0,0,.21.33l4,4a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L11.41,13H21a1,1,0,0,0,0-2Z"/></svg>
                                    </a>
                                    @endguest

                                </li>
                                <!-- replay comment -->
                                @foreach($comments as $commentC)
                                    @if($comment->id == $commentC->reply_id)
                                    <li class="comments__item comments__item--answer" id="{{$commentC->id}}">
                                        <div class="comments__autor">
                                        @foreach($users as $userC)
                                            @if($userC->id == $commentC->user_id )
                                                
                                            <img class="comments__avatar" src="{{ asset('assets/img/avatar.svg')}}" alt="">
                                            <span class="comments__name">{{$userC->name}}</span>
                                                
                                            @endif
                                        @endforeach
                                            <span class="comments__time">{{$commentC->created_at}}</span>
                                        </div>
                                        <p class="comments__text @if(session()->has('message'.$commentC->id)) alert alert-{{session('message'.$commentC->id)}} @endif">{!! $commentC->comment !!}</p>
                                        <div class="comments__actions">
                                            <!-- <div class="comments__rate">
                                                <button type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg> 12</button>

                                                <button type="button">7 <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H8a1,1,0,0,0,0,2h8a1,1,0,0,0,0-2Z"/></svg></button>
                                            </div> --> 
                                            <!-- condition for comment woner to permission delte -->
                                            @if(optional(auth()->user())->id == $commentC->user_id)   
                                            <div class="comments__rate">
                                                <form action="{{ url('comments/') }}/{{ $commentC->id }}" method="POST">
                                                {{ csrf_field() }}
                                                @method('DELETE')
                                                <input type="hidden" name="post_id" value="{{ $comment->post_id }}"/>                                           
                                                <button type="submit"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0 0 30 30" style=" fill:#000000;">    <path d="M 14.984375 2.4863281 A 1.0001 1.0001 0 0 0 14 3.5 L 14 4 L 8.5 4 A 1.0001 1.0001 0 0 0 7.4863281 5 L 6 5 A 1.0001 1.0001 0 1 0 6 7 L 24 7 A 1.0001 1.0001 0 1 0 24 5 L 22.513672 5 A 1.0001 1.0001 0 0 0 21.5 4 L 16 4 L 16 3.5 A 1.0001 1.0001 0 0 0 14.984375 2.4863281 z M 6 9 L 7.7929688 24.234375 C 7.9109687 25.241375 8.7633438 26 9.7773438 26 L 20.222656 26 C 21.236656 26 22.088031 25.241375 22.207031 24.234375 L 24 9 L 6 9 z"></path></svg></button></form>
                                            </div>
                                            @endif
                                            <script>
                                                function changeValue{{$commentC->id}}(o){
                                                    document.getElementById('type{{$commentC->id}}').value=o;
                                                    document.getElementById("replay{{$commentC->id}}").style.display = "block";
                                                    document.getElementById('typeQuote{{$commentC->id}}').value="<b>{!! $commentC->comment !!}</b>";
                                                }
                                            </script>

                                            <button onclick="changeValue{{$commentC->id}}('{{$comment->id}}')"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M21.707,11.293l-8-8A.99991.99991,0,0,0,12,4V7.54492A11.01525,11.01525,0,0,0,2,18.5V20a1,1,0,0,0,1.78418.62061,11.45625,11.45625,0,0,1,7.88672-4.04932c.0498-.00635.1748-.01611.3291-.02588V20a.99991.99991,0,0,0,1.707.707l8-8A.99962.99962,0,0,0,21.707,11.293ZM14,17.58594V15.5a.99974.99974,0,0,0-1-1c-.25488,0-1.2959.04932-1.56152.085A14.00507,14.00507,0,0,0,4.05176,17.5332,9.01266,9.01266,0,0,1,13,9.5a.99974.99974,0,0,0,1-1V6.41406L19.58594,12Z"/></svg><span >Reply</span></button>
                                            <!-- <button type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,14a1,1,0,0,0-1.22.72A7,7,0,0,1,11,20H5.41l.64-.63a1,1,0,0,0,0-1.41A7,7,0,0,1,9.25,6.22a1,1,0,0,0-.5-1.94A9,9,0,0,0,4,18.62L2.29,20.29a1,1,0,0,0-.21,1.09A1,1,0,0,0,3,22h8a9,9,0,0,0,8.72-6.75A1,1,0,0,0,19,14ZM21,2a1,1,0,0,0-1,1h0a5,5,0,1,0,.3,7.75A1,1,0,1,0,19,9.25,3,3,0,1,1,17,4a3,3,0,0,1,2.23,1H18a1,1,0,0,0,0,2h3a1,1,0,0,0,1-1V3A1,1,0,0,0,21,2Z"/></svg><span>Quote</span></button>-->                                        

                                        </div>
                                        @auth()
                                        <form action="{{ url('/comments') }}" method="post" enctype="multipart/form-data" class="comments__form displayNone" id="replay{{$commentC->id}}">
                                            {{ csrf_field() }}
                                            <div class="sign__group">
                                                <input type="hidden" name="post_id" value="{{ $post->id}}" class="sign__input" placeholder="{{ $post->id}}"> 
                                                <input type="hidden" name="user_id" value="{{ optional(auth()->user())->id}}" class="sign__input" placeholder="{{ optional(auth()->user())->id}}"> 
                                                <input type="hidden" id="type{{$commentC->id}}" name="reply_id" class="sign__input" placeholder="reply_id">                                            
                                                <input type="hidden" id="typeQuote{{$commentC->id}}" name="changeQuote" class="sign__input" placeholder="Quite Comment">  <!-- replay comment with quite --> 
                                                <textarea id="text" name="comment" class="sign__textarea" placeholder="Add comment"></textarea>
                                            </div>
                                            <button type="submit" class="sign__btn">Send</button>
                                        </form>                                                                          
                                        @endauth
                                        @guest()
                                        <a class="header__action-btn" href="{{ url('/signin')}}">
                                            <span>Sign in for comment reply</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.5,15.1a1,1,0,0,0-1.34.45A8,8,0,1,1,12,4a7.93,7.93,0,0,1,7.16,4.45,1,1,0,0,0,1.8-.9,10,10,0,1,0,0,8.9A1,1,0,0,0,20.5,15.1ZM21,11H11.41l2.3-2.29a1,1,0,1,0-1.42-1.42l-4,4a1,1,0,0,0-.21.33,1,1,0,0,0,0,.76,1,1,0,0,0,.21.33l4,4a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L11.41,13H21a1,1,0,0,0,0-2Z"/></svg>
                                        </a>
                                        @endguest


                                    </li>
                                    @endif                                
                                @endforeach
                            @endif
                        @endforeach
                    </ul>
                    @auth()
                    <form action="{{ url('/comments') }}" method="post" enctype="multipart/form-data" class="comments__form">
                    {{ csrf_field() }}
                        <div class="sign__group">
                            <input type="hidden" name="post_id" value="{{ $post->id}}" class="sign__input" placeholder="{{ $post->id}}"> 
                            <input type="hidden" name="user_id" value="{{ optional(auth()->user())->id}}" class="sign__input" placeholder="{{ optional(auth()->user())->id}}"> 
                            <input type="hidden" name="reply_id" class="sign__input" placeholder="reply_id"> 
                            <textarea id="text" name="comment" class="sign__textarea" placeholder="Add comment"></textarea>
                        </div>
                        <button type="submit" class="sign__btn">Send</button>
                    </form>                                                                       
                    @endauth
                    @guest()
                    <a class="header__action-btn" href="{{ url('/signin')}}">
                        <span>Sign in for comments</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.5,15.1a1,1,0,0,0-1.34.45A8,8,0,1,1,12,4a7.93,7.93,0,0,1,7.16,4.45,1,1,0,0,0,1.8-.9,10,10,0,1,0,0,8.9A1,1,0,0,0,20.5,15.1ZM21,11H11.41l2.3-2.29a1,1,0,1,0-1.42-1.42l-4,4a1,1,0,0,0-.21.33,1,1,0,0,0,0,.76,1,1,0,0,0,.21.33l4,4a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L11.41,13H21a1,1,0,0,0,0-2Z"/></svg>
                    </a>
                    @endguest
                </div>
                <!-- end comments -->
            </div>
        </section>

        <!-- recommend -->
        <!-- <section class="row row--grid">
           
            <div class="col-12">
                <div class="main__title">
                    <h2>Recommend</h2>

                    <a href="store.html" class="main__link">See all <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></a>
                </div>
            </div>
           

            <div class="col-12">
                <div class="main__carousel-wrap">
                    <div class="main__carousel main__carousel--store owl-carousel" id="store">
                        <div class="product">
                            <div class="product__img">
                                <img src="img/store/item1.jpg" alt="">

                                <button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
                            </div>
                            <h3 class="product__title"><a href="product.html">Vinyl Player</a></h3>
                            <span class="product__price">$1 099</span>
                            <span class="product__new">New</span>
                        </div>

                        <div class="product">
                            <div class="product__img">
                                <img src="img/store/item2.jpg" alt="">

                                <button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
                            </div>
                            <h3 class="product__title"><a href="product.html">Microphone R4</a></h3>
                            <span class="product__price">$799</span>
                        </div>

                        <div class="product">
                            <div class="product__img">
                                <img src="img/store/item3.jpg" alt="">

                                <button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
                            </div>
                            <h3 class="product__title"><a href="product.html">Music Blank</a></h3>
                            <span class="product__price">$3.99</span>
                            <span class="product__new">New</span>
                        </div>

                        <div class="product">
                            <div class="product__img">
                                <img src="img/store/item4.jpg" alt="">

                                <button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
                            </div>
                            <h3 class="product__title"><a href="product.html">Headphones ZR-991</a></h3>
                            <span class="product__price">$199</span>
                        </div>

                        <div class="product">
                            <div class="product__img">
                                <img src="img/store/item5.jpg" alt="">

                                <button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
                            </div>
                            <h3 class="product__title"><a href="product.html">Piano</a></h3>
                            <span class="product__price">$11 899</span>
                        </div>

                        <div class="product">
                            <div class="product__img">
                                <img src="img/store/item6.jpg" alt="">

                                <button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
                            </div>
                            <h3 class="product__title"><a href="product.html">Vinyl Player</a></h3>
                            <span class="product__price">$2 499</span>
                        </div>

                        <div class="product">
                            <div class="product__img">
                                <img src="img/store/item7.jpg" alt="">

                                <button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
                            </div>
                            <h3 class="product__title"><a href="product.html">Guitar</a></h3>
                            <span class="product__price">$299</span>
                        </div>

                        <div class="product">
                            <div class="product__img">
                                <img src="img/store/item8.jpg" alt="">

                                <button class="product__add" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4-9H13V8a1,1,0,0,0-2,0v3H8a1,1,0,0,0,0,2h3v3a1,1,0,0,0,2,0V13h3a1,1,0,0,0,0-2Z"/></svg></button>
                            </div>
                            <h3 class="product__title"><a href="product.html">Microphone R4s</a></h3>
                            <span class="product__price">$199</span>
                        </div>
                    </div>

                    <button class="main__nav main__nav--prev" data-nav="#store" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,11H9.41l3.3-3.29a1,1,0,1,0-1.42-1.42l-5,5a1,1,0,0,0-.21.33,1,1,0,0,0,0,.76,1,1,0,0,0,.21.33l5,5a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42L9.41,13H17a1,1,0,0,0,0-2Z"/></svg></button>
                    <button class="main__nav main__nav--next" data-nav="#store" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17.92,11.62a1,1,0,0,0-.21-.33l-5-5a1,1,0,0,0-1.42,1.42L14.59,11H7a1,1,0,0,0,0,2h7.59l-3.3,3.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l5-5a1,1,0,0,0,.21-.33A1,1,0,0,0,17.92,11.62Z"/></svg></button>
                </div>
            </div>
        </section> -->
        <!-- end recommend -->
    </div>
</main>
<!-- end main content -->

@endsection