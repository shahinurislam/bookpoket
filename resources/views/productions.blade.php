@extends('master')
@section('content')
	<!-- main content -->
	<main class="main">
		<div class="container-fluid">
			<!-- artists -->
			<div class="row row--grid">
				<!-- breadcrumb -->
				<div class="col-12">
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">Productions</li>
					</ul>
				</div>
				<!-- end breadcrumb -->

				<!-- title -->
				<div class="col-12">                    
                    <!-- sidebar nav -->
                    <ul class="sidebar__nav">

                        <!-- Artists -->
                        <li class="sidebar__nav-item">
                            <a class="sidebar__nav-link" data-toggle="collapse" href="#collapseMenu1" role="button" aria-expanded="false" aria-controls="collapseMenu1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,5.5H12.72l-.32-1a3,3,0,0,0-2.84-2H5a3,3,0,0,0-3,3v13a3,3,0,0,0,3,3H19a3,3,0,0,0,3-3V8.5A3,3,0,0,0,19,5.5Zm1,13a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V5.5a1,1,0,0,1,1-1H9.56a1,1,0,0,1,.95.68l.54,1.64A1,1,0,0,0,12,7.5h7a1,1,0,0,1,1,1Z"/></svg> <span>Artists</span> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,9.17a1,1,0,0,0-1.41,0L12,12.71,8.46,9.17a1,1,0,0,0-1.41,0,1,1,0,0,0,0,1.42l4.24,4.24a1,1,0,0,0,1.42,0L17,10.59A1,1,0,0,0,17,9.17Z"/></svg></a>
                            <div class="collapse" id="collapseMenu1">
                                <ul class="sidebar__menu sidebar__menu--scroll">
                                    <li><a href="{{ url('/dashboard/artists') }}">All Artists</a></li>
                                    <li><a href="{{ url('/dashboard/artist/create') }}">Add New</a></li>
                                </ul>
                            </div>
                        </li> 
                        <!-- Releases -->
                        <li class="sidebar__nav-item">
                            <a class="sidebar__nav-link" data-toggle="collapse" href="#collapseMenu2" role="button" aria-expanded="false" aria-controls="collapseMenu2"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,5.5H12.72l-.32-1a3,3,0,0,0-2.84-2H5a3,3,0,0,0-3,3v13a3,3,0,0,0,3,3H19a3,3,0,0,0,3-3V8.5A3,3,0,0,0,19,5.5Zm1,13a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V5.5a1,1,0,0,1,1-1H9.56a1,1,0,0,1,.95.68l.54,1.64A1,1,0,0,0,12,7.5h7a1,1,0,0,1,1,1Z"/></svg> <span>Releases</span> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,9.17a1,1,0,0,0-1.41,0L12,12.71,8.46,9.17a1,1,0,0,0-1.41,0,1,1,0,0,0,0,1.42l4.24,4.24a1,1,0,0,0,1.42,0L17,10.59A1,1,0,0,0,17,9.17Z"/></svg></a>
                            <div class="collapse" id="collapseMenu2">
                                <ul class="sidebar__menu sidebar__menu--scroll">
                                    <li><a href="{{ url('/dashboard/releases') }}">All Releases</a></li>
                                    <li><a href="{{ url('/dashboard/release/create') }}">Add New</a></li>
                                </ul>
                            </div>
                        </li>  
                        <!-- Banners -->
                        <li class="sidebar__nav-item">
                            <a class="sidebar__nav-link" data-toggle="collapse" href="#collapseMenuBanner" role="button" aria-expanded="false" aria-controls="collapseMenuBanner"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,5.5H12.72l-.32-1a3,3,0,0,0-2.84-2H5a3,3,0,0,0-3,3v13a3,3,0,0,0,3,3H19a3,3,0,0,0,3-3V8.5A3,3,0,0,0,19,5.5Zm1,13a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V5.5a1,1,0,0,1,1-1H9.56a1,1,0,0,1,.95.68l.54,1.64A1,1,0,0,0,12,7.5h7a1,1,0,0,1,1,1Z"/></svg> <span>Banners</span> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,9.17a1,1,0,0,0-1.41,0L12,12.71,8.46,9.17a1,1,0,0,0-1.41,0,1,1,0,0,0,0,1.42l4.24,4.24a1,1,0,0,0,1.42,0L17,10.59A1,1,0,0,0,17,9.17Z"/></svg></a>
                            <div class="collapse" id="collapseMenuBanner">
                                <ul class="sidebar__menu sidebar__menu--scroll">
                                    <li><a href="{{ url('/dashboard/banners') }}">All Banners</a></li>
                                    <li><a href="{{ url('/dashboard/banner/create') }}">Add New</a></li>
                                </ul>
                            </div>
                        </li> 
                        <!-- Posts -->
                        <li class="sidebar__nav-item">
                            <a class="sidebar__nav-link" data-toggle="collapse" href="#collapseMenu3" role="button" aria-expanded="false" aria-controls="collapseMenu3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,5.5H12.72l-.32-1a3,3,0,0,0-2.84-2H5a3,3,0,0,0-3,3v13a3,3,0,0,0,3,3H19a3,3,0,0,0,3-3V8.5A3,3,0,0,0,19,5.5Zm1,13a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V5.5a1,1,0,0,1,1-1H9.56a1,1,0,0,1,.95.68l.54,1.64A1,1,0,0,0,12,7.5h7a1,1,0,0,1,1,1Z"/></svg> <span>Posts</span> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,9.17a1,1,0,0,0-1.41,0L12,12.71,8.46,9.17a1,1,0,0,0-1.41,0,1,1,0,0,0,0,1.42l4.24,4.24a1,1,0,0,0,1.42,0L17,10.59A1,1,0,0,0,17,9.17Z"/></svg></a>
                            <div class="collapse" id="collapseMenu3">
                                <ul class="sidebar__menu sidebar__menu--scroll">
                                    <li><a href="{{ url('/dashboard/posts') }}">All Posts</a></li>
                                    <li><a href="{{ url('/dashboard/post/create') }}">Add New</a></li>
                                </ul>
                            </div>
                        </li> 
                        
                        <!-- Pages -->
                        <li class="sidebar__nav-item">
                            <a class="sidebar__nav-link" data-toggle="collapse" href="#collapsePages" role="button" aria-expanded="false" aria-controls="collapsePages"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,5.5H12.72l-.32-1a3,3,0,0,0-2.84-2H5a3,3,0,0,0-3,3v13a3,3,0,0,0,3,3H19a3,3,0,0,0,3-3V8.5A3,3,0,0,0,19,5.5Zm1,13a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V5.5a1,1,0,0,1,1-1H9.56a1,1,0,0,1,.95.68l.54,1.64A1,1,0,0,0,12,7.5h7a1,1,0,0,1,1,1Z"/></svg> <span>Pages</span> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M17,9.17a1,1,0,0,0-1.41,0L12,12.71,8.46,9.17a1,1,0,0,0-1.41,0,1,1,0,0,0,0,1.42l4.24,4.24a1,1,0,0,0,1.42,0L17,10.59A1,1,0,0,0,17,9.17Z"/></svg></a>
                            <div class="collapse" id="collapsePages">
                                <ul class="sidebar__menu sidebar__menu--scroll">
                                    <li><a href="{{ url('/dashboard/pages') }}">All Pages</a></li>
                                    <li><a href="{{ url('/dashboard/page/create') }}">Add New</a></li>
                                </ul>
                            </div>
                        </li>  
                        
                        
                    </ul>
                    <!-- end sidebar nav -->		
				</div>
				<!-- end title -->
			</div>
		</div>
	</main>
	<!-- end main content -->
@endsection