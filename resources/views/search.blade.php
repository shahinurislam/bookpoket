@extends('master')
@section('content')
	<!-- main content -->
	<main class="main">
		<div class="container-fluid">
			<div class="row row--grid">
				<!-- breadcrumb -->
				<div class="col-12">
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">Search Result:</li>
					</ul>
				</div>
				<!-- end breadcrumb -->

				<!-- title -->
                <div class="col-12">
                    <ul class="main__list">
                    <!-- not found  -->
                    @if($search_posts == "[]")
                    <section class="fasality-pag">
                        <div class="container">
                        <!--Start bottom text-->
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="sec-title">
                                    <h2>Nothing Found <span></span></h2>
                                    <span class="decor"></span>
                                    </div>
                                    <p class="facilities-body">Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
                                </div>
                            </div>
                        </div>
                    </section>
                    @endif
                    <!-- not found  -->

                    @php $count = 1; @endphp
                    @foreach($search_posts as $post)
                        @if($post->status == 1)
                        <li class="single-item">
                            <span class="single-item__number">{{ $count }}</span>
                            <!-- count -->
                            @if($loop->iteration % 2 == 0)										
                                <span class="single-item__rate"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12.71,12.54a1,1,0,0,0-1.42,0l-3,3A1,1,0,0,0,9.71,17L12,14.66,14.29,17a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42Zm-3-1.08L12,9.16l2.29,2.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42l-3-3a1,1,0,0,0-1.42,0l-3,3a1,1,0,0,0,1.42,1.42Z"/></svg> {{ $count }}</span>
                            @else
                                <span class="single-item__rate single-item__rate--red"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.29,11.46a1,1,0,0,0,1.42,0l3-3A1,1,0,1,0,14.29,7L12,9.34,9.71,7A1,1,0,1,0,8.29,8.46Zm3,1.08L12,14.84l-2.29-2.3A1,1,0,0,0,8.29,14l3,3a1,1,0,0,0,1.42,0l3-3a1,1,0,0,0-1.42-1.42Z"/></svg> {{ $count }}</span>
                            @endif
                            <!-- count -->
                            <a data-link data-title="{{ $post->title}}"

                            @foreach($artists as $artist)
                                @if($artist->id == $post->artist_id)
                                    data-artist="{{ $artist->title }}"                                      
                                    data-img="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}"                                   
                                @endif
                            @endforeach	
                            
                            href="{{ $post->audio_path_url == null ? asset('audio').'/'.$post->audio_path : $post->audio_path_url }}" class="update{{$post->id}} single-item__cover">

                            @foreach($artists as $artist)
                                @if($artist->id == $post->artist_id)
                                <img src="{{ asset('/images/artists/')}}/{{ $artist->thumbnail_path }}" alt="sdsd">
                                @endif
                            @endforeach	
                                <!-- <img src="{{ asset('assets/img/covers/cover1.jpg')}}" alt=""> -->
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.54,9,8.88,3.46a3.42,3.42,0,0,0-5.13,3V17.58A3.42,3.42,0,0,0,7.17,21a3.43,3.43,0,0,0,1.71-.46L18.54,15a3.42,3.42,0,0,0,0-5.92Zm-1,4.19L7.88,18.81a1.44,1.44,0,0,1-1.42,0,1.42,1.42,0,0,1-.71-1.23V6.42a1.42,1.42,0,0,1,.71-1.23A1.51,1.51,0,0,1,7.17,5a1.54,1.54,0,0,1,.71.19l9.66,5.58a1.42,1.42,0,0,1,0,2.46Z"/></svg>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M16,2a3,3,0,0,0-3,3V19a3,3,0,0,0,6,0V5A3,3,0,0,0,16,2Zm1,17a1,1,0,0,1-2,0V5a1,1,0,0,1,2,0ZM8,2A3,3,0,0,0,5,5V19a3,3,0,0,0,6,0V5A3,3,0,0,0,8,2ZM9,19a1,1,0,0,1-2,0V5A1,1,0,0,1,9,5Z"/></svg>
                            </a>
                            <!-- how many times are play  -->
                            <script type='text/javascript'>
                            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                                $(document).on("click", ".update{{$post->id}}" , function() {
                                    var edit_id = {{ $post->id}};

                                    var name = {{ $post->download_count}} + 1;
                                        $.ajax({
                                        url: '/dashboard/post/{{ $post->id}}',
                                        type: 'patch',
                                        data: {_token: CSRF_TOKEN,id: edit_id,download_count: name},
                                        success: function(response){
                                            alert(response);
                                        }
                                        });											 
                                    }); 
                                </script>
                            <!-- how many times are play  -->
                            
                            <div class="single-item__title">
                                <h4><a href="{{ url('post/listen/'.$post->id) }}">{{ substr($post->title, 0, 20)}}</a></h4>
                                <span>
                                @foreach($artists as $artist)
                                    @if($artist->id == $post->artist_id)
                                    <a href="{{ url('artist/'.$artist->id) }}">
                                        {{ $artist->title }}
                                    </a>
                                    @endif
                                @endforeach 
                                </span>
                            </div>
                            <span class="single-item__time">{{ $post->duration}}</span>
                        </li>
                        @php $count+= 1; @endphp
                        @endif
                    @endforeach 
                        
                    </ul>
                </div>
				<!-- end title -->

			</div>
		</div>
	</main>
	<!-- end main content -->
@endsection