<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/postsdemo', [PostApiController::class, 'posts']);
Route::get('/allposts', [PostApiController::class, 'allposts']);
Route::get('/posts', [PostApiController::class, 'index']);
Route::get('/posts/{id}', [PostApiController::class, 'show']);
Route::post('/posts', [PostApiController::class, 'store']);
Route::put('/posts/{post}', [PostApiController::class, 'update']);
Route::delete('/posts/{post}', [PostApiController::class, 'destroy']);

Route::get('/artists', [PostApiController::class, 'artists']);
Route::get('/artists/{id}', [PostApiController::class, 'artistsShow']);

Route::get('/comments', [PostApiController::class, 'comments']);
Route::get('/comments/{id}', [PostApiController::class, 'commentsShow']);
Route::post('/comments', [PostApiController::class, 'commentsStore']);

Route::post('/signin', [PostApiController::class, 'processLogin']); 
Route::post('/signup', [PostApiController::class, 'prosessRegister']);

Route::get('/search/{name}', [PostApiController::class, 'search']); 
