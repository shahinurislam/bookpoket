<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArtistsController;
use App\Http\Controllers\ReleasesController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\BannersController;
use App\Http\Controllers\PagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index']);

Route::get('/artists', [PublicController::class, 'artists']);
Route::get('/artist/{id}', [PublicController::class, 'artistsShow']);

Route::get('/releases', [PublicController::class, 'releases']);
Route::get('/listen/{id}', [PublicController::class, 'listen']);
Route::get('/productions', [PublicController::class, 'productions']);
Route::get('/post/listen/{id}', [PublicController::class, 'listenShow']);

Route::get('/page/{slug}', [PublicController::class, 'pageShow']);

Route::get('login', [AuthController::class, 'showLoginForm'])->name('login');
//login registration
Route::get('/signup', [AuthController::class, 'showRegisterForm'])->name('signup');
Route::post('/signup', [AuthController::class, 'prosessRegister']);
Route::get('/signin', [AuthController::class, 'showLoginForm'])->name('signin');
Route::post('/signin', [AuthController::class, 'processLogin']);
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

//artists 
Route::get('/dashboard/artists', [ArtistsController::class, 'index'])->name('artists.index');
Route::get('/dashboard/artist/create', [ArtistsController::class, 'create']);
Route::post('/dashboard/artist', [ArtistsController::class, 'store']);
Route::get('/dashboard/artist/{id}', [ArtistsController::class, 'show']);
Route::get('/dashboard/artist/{id}/edit', [ArtistsController::class, 'edit']);
Route::patch('/dashboard/artist/{id}', [ArtistsController::class, 'update']);
Route::delete('/dashboard/artist/{id}', [ArtistsController::class, 'destroy']);

//releases 
Route::get('/dashboard/releases', [ReleasesController::class, 'index'])->name('releases.index');
Route::get('/dashboard/release/create', [ReleasesController::class, 'create']);
Route::post('/dashboard/release', [ReleasesController::class, 'store']);
Route::get('/dashboard/release/{id}', [ReleasesController::class, 'show']);
Route::get('/dashboard/release/{id}/edit', [ReleasesController::class, 'edit']);
Route::patch('/dashboard/release/{id}', [ReleasesController::class, 'update']);
Route::delete('/dashboard/release/{id}', [ReleasesController::class, 'destroy']);

//posts 
Route::get('/dashboard/posts', [PostsController::class, 'index'])->name('posts.index');
Route::get('/dashboard/post/create', [PostsController::class, 'create']);
Route::post('/dashboard/post', [PostsController::class, 'store']);
Route::get('/dashboard/post/{id}', [PostsController::class, 'show']);
Route::get('/dashboard/post/{id}/edit', [PostsController::class, 'edit']);
Route::patch('/dashboard/post/{id}', [PostsController::class, 'update']);
Route::delete('/dashboard/post/{id}', [PostsController::class, 'destroy']);
//post update for ajax
Route::patch('/dashboardajax/post/{id}', [PostsController::class, 'updateajax']);

//comments 
Route::post('/comments', [CommentsController::class, 'store']);
Route::get('/comments/{id}/edit', [CommentsController::class, 'edit']);
Route::patch('/comments/{id}', [CommentsController::class, 'update']);
Route::delete('/comments/{id}', [CommentsController::class, 'destroy']);

//banners 
Route::get('/dashboard/banners', [BannersController::class, 'index'])->name('banners.index');
Route::get('/dashboard/banner/create', [BannersController::class, 'create']);
Route::post('/dashboard/banner', [BannersController::class, 'store']);
Route::get('/dashboard/banner/{id}', [BannersController::class, 'show']);
Route::get('/dashboard/banner/{id}/edit', [BannersController::class, 'edit']);
Route::patch('/dashboard/banner/{id}', [BannersController::class, 'update']);
Route::delete('/dashboard/banner/{id}', [BannersController::class, 'destroy']);

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear'); 
    Artisan::call('route:clear'); 
    Artisan::call('route:cache'); 
    \Artisan::call('route:clear');
    return "Cleared!"; 
 });

//pages 
Route::get('/dashboard/pages', [PagesController::class, 'index'])->name('pages.index');
Route::get('/dashboard/page/create', [PagesController::class, 'create']);
Route::post('/dashboard/page', [PagesController::class, 'store']);
//Route::get('/dashboard/page/{slug}', [PagesController::class, 'show']);
Route::get('/dashboard/page/{id}/edit', [PagesController::class, 'edit']);
Route::patch('/dashboard/page/{id}', [PagesController::class, 'update']);
Route::delete('/dashboard/page/{id}', [PagesController::class, 'destroy']);

Route::get('/search', [PublicController::class, 'search']); 